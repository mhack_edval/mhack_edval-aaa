﻿using System.Reflection;

namespace Edval.OS.AAA.Tools
{
    public interface IAAAConfiguration
    {
        public string ApiKey { get; }
        public string RoutePathPrefix { get; }
        public string AuthServiceUrl { get; }
        public string Version { get; }
        public Assembly Assembly { get; }
        public bool OptionsPoliciesPublicByDefault { get; }
    }
}
