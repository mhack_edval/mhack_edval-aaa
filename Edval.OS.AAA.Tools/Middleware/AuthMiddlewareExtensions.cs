﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Edval.OS.AAA.Tools.Middleware
{
    public static class AuthMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthMiddleware(this IApplicationBuilder app, IAAAConfiguration aaaConfiguration = null)
        {
            var config = aaaConfiguration ?? 
                app.ApplicationServices.GetRequiredService<IAAAConfiguration>() ?? 
                throw new Exception($"You must either register a {nameof(IAAAConfiguration)} service or pass the {nameof(aaaConfiguration)} in the {nameof(UseAuthMiddleware)} call.");
            app.UseMiddleware<AuthMiddleware>(config);
            return app;
        }
    }
}