﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Edval.OS.AAA.Client.Api;
using Edval.OS.AAA.Client.Client;
using Edval.OS.AAA.Client.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace Edval.OS.AAA.Tools.Middleware
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string _authUrlBasePath;
        private readonly ILogger<AuthMiddleware> _logger;
        private readonly AuthApi _api;

        public AuthMiddleware(RequestDelegate next, IAAAConfiguration configuration, ILogger<AuthMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _ = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _ = configuration.AuthServiceUrl ?? throw new ArgumentNullException(nameof(configuration) + "." + nameof(configuration.AuthServiceUrl));
            _authUrlBasePath = configuration.RoutePathPrefix ?? throw new ArgumentNullException(nameof(configuration) + "." + nameof(configuration.RoutePathPrefix));
            _logger = logger;

            if (!_authUrlBasePath.EndsWith('/'))
            {
                _authUrlBasePath += '/';
            }
            
            _api = new AuthApi(configuration.AuthServiceUrl);
            _api.ExceptionFactory = null; // don't throw on 400 errors - we will have to monitor the status code ourself though
        }

        public async Task InvokeAsync(HttpContext context)
        {
            Endpoint endPoint = context.GetEndpoint();

            string? path;
            if (endPoint == null) // probably file serving or similar
            {
                path = context.Request.Path;
                if (path?.StartsWith('/') == true)
                {
                    path = path.TrimStart('/');
                }
            }
            else
            {
                ControllerActionDescriptor controllerDescriptor =
                    endPoint.Metadata.GetMetadata<ControllerActionDescriptor>();
                path = controllerDescriptor?.AttributeRouteInfo?.Template ?? "";
            }

            RouteData data = context.GetRouteData();

            List<ApiClientKeyValue> pathParameterNames = GetAllPathParameterNames(path);
            for (int i = 0; i < pathParameterNames.Count; i++)
            {
                ApiClientKeyValue? x = pathParameterNames[i];
                x.Value = data.Values.TryGetValue(x.Key, out object val) ? val : null;
            }

            List<ApiClientKeyValue> headers = new List<ApiClientKeyValue>();
            foreach (var (key, value) in context.Request.Headers)
            {
                headers.Add(new ApiClientKeyValue(key, value.FirstOrDefault()));
            }

            List<ApiClientKeyValue> queries = new List<ApiClientKeyValue>();
            foreach (var (key, value) in context.Request.Query)
            {
                queries.Add(new ApiClientKeyValue(key, value.FirstOrDefault()));
            }

            List<ApiClientKeyValue> cookies = new List<ApiClientKeyValue>();
            foreach (var (key, value) in context.Request.Cookies)
            {
                cookies.Add(new ApiClientKeyValue(key, value));
            }

            if (!Enum.TryParse(context.Request.Method, out ApiClientHttpMethod method))
            {
                throw new Exception("Unable to determine method!");
            }

            ApiClientAuthorisationRequest authRequest = new ApiClientAuthorisationRequest(pathParameterNames, headers, queries, cookies, _authUrlBasePath + path, method);
            ApiClientAuthorisationResponse result = await MakeAuthRequest(authRequest);
            
            if (result.Success)
            {
                // set the context user if necessary
                if (!result.UserId.IsNullOrEmpty() || !result.TenantId.IsNullOrEmpty())
                {
                    List<Claim> claims = new List<Claim>
                    {
                        new Claim(AuthMiddlewareClaimsHelper.UserId, result.UserId),
                        new Claim(AuthMiddlewareClaimsHelper.TenantId, result.TenantId)
                    };

                    ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);
                    ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);
                    context.User = principal;
                }

                using (_logger.BeginScope("User {user}, tenant {tenant}", result.UserId, result.TenantId))
                {
                    await _next(context);
                }
            }
            else // unauthorised
            {
                _logger.LogInformation("Failed authorising request - URL: {Url}, METHOD: {HttpMethod}, REASON: {Details}", 
                    authRequest.Url, authRequest.HttpMethod, result.Details);
                await WriteUnauthorised(context, result);
                return;
            }
        }

        private async Task WriteUnauthorised(HttpContext context, ApiClientAuthorisationResponse? result)
        {
            context.Response.SetDontCacheHeader();
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            await using TextWriter writer = new StreamWriter(context.Response.Body);
            await writer.WriteAsync("Unauthorised: " + result?.Details);
        }

        private bool ShouldRetryStatusCode(int statusCode)
        {
            return statusCode switch
            {
                200 => false,
                400 => false,
                401 => false,
                403 => false,
                _ => true
            };
        }
        
        private async Task<ApiClientAuthorisationResponse> MakeAuthRequest(ApiClientAuthorisationRequest request)
        {
            const int maxRetries = 3;

            for (int i = 0; i < maxRetries; i++)
            {
                try
                {
                    ApiResponse<ApiClientAuthorisationResponse> result = await _api.AuthoriseWithHttpInfoAsync(request);

                    if (result.Data == null && i < maxRetries - 1 && ShouldRetryStatusCode(result.StatusCode))
                    {
                        continue;
                    }

                    if(result.Data == null)
                    {
                        return new ApiClientAuthorisationResponse(false,
                            "Unknown failure. Status code: " + result.StatusCode, "", "");
                    }

                    return result.Data;
                }
                catch (Exception e)
                {
                    if (i == maxRetries - 1)
                    {
                        throw;
                    }

                    _logger.LogError(e, "Failed making AAA API call");

                    await Task.Delay(50 * (i + 1));
                }
            }

            throw new Exception("Should be unreachable. Failed request auth but didn't throw on last attempt.");
        }

        public static List<ApiClientKeyValue> GetAllPathParameterNames(string path)
        {
            List<ApiClientKeyValue> pathParameterNames = new List<ApiClientKeyValue>();

            if (path.IsNullOrEmpty())
            {
                return pathParameterNames;
            }

            int startIndex = -1;
            for (int i = 0; i < path.Length; i++)
            {
                switch (path[i])
                {
                    case '{':
                        startIndex = i + 1;
                        i++;
                        continue;
                    case '}':
                        pathParameterNames.Add(new ApiClientKeyValue(path.Substring(startIndex, i - startIndex), null));
                        break;
                }
            }

            return pathParameterNames;
        }
    }
}
