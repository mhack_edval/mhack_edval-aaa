﻿namespace Edval.OS.AAA.Tools.Middleware
{
    public static class AuthMiddlewareClaimsHelper
    {
        public static readonly string TenantId = "TenantId";
        public static readonly string UserId = "UserId";
    }
}
