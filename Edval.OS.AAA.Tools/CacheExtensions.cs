﻿using Microsoft.AspNetCore.Http;

namespace Edval.OS.AAA.Tools
{
    internal static class CacheExtensions
    {
        public static HttpResponse SetCacheControlHeader(this HttpResponse response, string value)
        {
            response.Headers["Cache-Control"] = value;
            return response;
        }

        public static HttpResponse SetSurrogateControlHeader(this HttpResponse response, string value)
        {
            response.Headers["Surrogate-Control"] = value;
            return response;
        }

        public static HttpResponse SetDontCacheHeader(this HttpResponse response)
        {
            response.Headers["Cache-Control"] = "private, no-store";
            return response;
        }
    }
}