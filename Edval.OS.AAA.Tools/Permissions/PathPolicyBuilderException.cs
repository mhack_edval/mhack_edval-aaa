﻿using System;

namespace Edval.OS.AAA.Tools.Permissions
{
    public class PathPolicyBuilderException : Exception
    {
        public PathPolicyBuilderException(string msg) : base(msg)
        {
        }
    }
}