﻿using System;

namespace Edval.OS.AAA.Tools.Permissions
{
    /// <summary>
    /// Sets all actions on this controller to public unless they have an action specific permission attribute.
    /// </summary>
    [AttributeUsage(validOn: AttributeTargets.Class, AllowMultiple = false)]
    public class ControllerPublicByDefaultAttribute : Attribute
    {
        /// <summary>
        /// Sets all actions on this controller to public unless they have an action specific permission attribute.
        /// </summary>
        public ControllerPublicByDefaultAttribute()
        {
        }
    }
}
