﻿using System;
using Edval.OS.AAA.Client.Model;

namespace Edval.OS.AAA.Tools.Permissions
{
    [AttributeUsage(validOn: AttributeTargets.Method, AllowMultiple = true)]
    public class ActionRequiredPermissionAttribute : Attribute
    {
        public string PermissionPath { get; }
        public ApiClientPermission? Permission { get; }

        /// <summary>
        /// The required permissions on this action. Controller defaults will not apply.
        /// </summary>
        /// <param name="permissionPath">The required permission.</param>
        /// <param name="permission">Not setting the permission will default to the default for that http method type.</param>
        public ActionRequiredPermissionAttribute(string permissionPath, ApiClientPermission permission = ApiClientPermission.None)
        {
            PermissionPath = permissionPath;
            Permission = permission == ApiClientPermission.None ? (ApiClientPermission?)null : permission;
        }
    }
}
