﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Edval.OS.AAA.Client.Model;
using Microsoft.AspNetCore.Mvc;

namespace Edval.OS.AAA.Tools.Permissions
{
    public static class PathPolicyBuilder
    {
        public static List<ApiClientPathPolicy> GatherAndBuild(Assembly assembly, string version, string pathPrefix, bool optionsPoliciesPublicByDefault = true)
        {
            if (!pathPrefix.EndsWith('/'))
            {
                pathPrefix += "/";
            }

            List<ApiClientPathPolicyVariable> defaultPathPolicyVariables = new List<ApiClientPathPolicyVariable>()
            {
                { new ApiClientPathPolicyVariable("TenantId", ApiClientPathPolicyVariable.LocationEnum.Header, "TenantId") }
            };

            Dictionary<string, ApiClientPathPolicy> pathPolicies = new Dictionary<string, ApiClientPathPolicy>();
            foreach (Type type in assembly.GetTypes())
            {
                var methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public).ToList();
                foreach (MethodInfo method in methods)
                {
                    var publicAttribute = method.GetCustomAttribute<ActionPublicAttribute>();
                    var requiredPermissionsAttributes = method.GetCustomAttributes<ActionRequiredPermissionAttribute>().ToList();

                    if (publicAttribute == null && !requiredPermissionsAttributes.Any())
                    {
                        continue;
                    }

                    if (publicAttribute != null && requiredPermissionsAttributes.Any())
                    {
                        throw new PathPolicyBuilderException("Both public and required permissions can't be assigned to the same method. That doesn't make sense!");
                    }

                    if (!method.Name.EndsWith("Inner"))
                    {
                        throw new PathPolicyBuilderException(
                            "Permissions are only supported on auto generated controller methods.");
                    }

                    var controllerMethod = methods.FirstOrDefault(x =>
                        x.Name == method.Name.Substring(0, method.Name.Length - "Inner".Length));

                    if (controllerMethod == null)
                    {
                        throw new PathPolicyBuilderException(
                            $"Couldn't find the auto gen controller method for method {method.Name}.");
                    }

                    var route = controllerMethod.GetCustomAttribute<RouteAttribute>();
                    if (route == null)
                    {
                        throw new PathPolicyBuilderException(
                            $"Couldn't find the route attribute for method {method.Name}.");
                    }

                    string path = route.Template;
                    while (path.StartsWith('/'))
                    {
                        path = path.Substring(1);
                    }
                    path = pathPrefix + path;

                    if (!pathPolicies.TryGetValue(path, out ApiClientPathPolicy policy))
                    {
                        policy = new ApiClientPathPolicy(version, path, defaultPathPolicyVariables,
                            new List<ApiClientPathPolicyMethod>());
                        pathPolicies.Add(path, policy);
                    }

                    var httpMethod = MethodToHttpMethod(controllerMethod);
                    if (policy.PathPolicyMethods.Any(x => x.HttpMethod == httpMethod))
                    {
                        throw new PathPolicyBuilderException($"Cannot have duplicate http methods for {path}.");
                    }

                    if (publicAttribute != null)
                    {
                        policy.PathPolicyMethods.Add(new ApiClientPathPolicyMethod(httpMethod, new List<ApiClientRequirement>(), true));
                    }
                    else if (requiredPermissionsAttributes.Any())
                    {
                        var requirements = requiredPermissionsAttributes
                            .Select(x => new ApiClientRequirement(x.PermissionPath, x.Permission ?? HttpMethodToPermissionType(httpMethod)))
                            .ToList();
                        policy.PathPolicyMethods.Add(new ApiClientPathPolicyMethod(httpMethod, requirements, false));
                    }
                }

                var controllerPublicByDefault = type.GetCustomAttribute<ControllerPublicByDefaultAttribute>();
                var controllerDefaultRequiredPermissions = type.GetCustomAttributes<ControllerDefaultRequiredPermissionAttribute>().ToList();

                if (controllerPublicByDefault == null && !controllerDefaultRequiredPermissions.Any())
                {
                    continue;
                }

                if (controllerPublicByDefault != null && controllerDefaultRequiredPermissions.Any())
                {
                    throw new PathPolicyBuilderException(
                        "Both public and required permissions can't be assigned to the same controller. That doesn't make sense!");
                }

                foreach (MethodInfo method in methods)
                {
                    var route = method.GetCustomAttribute<RouteAttribute>();
                    if (route == null)
                    {
                        continue;
                    }

                    string path = route.Template;
                    while (path.StartsWith('/'))
                    {
                        path = path.Substring(1);
                    }
                    path = pathPrefix + path;

                    if (!pathPolicies.TryGetValue(path, out ApiClientPathPolicy policy))
                    {
                        policy = new ApiClientPathPolicy(version, path, defaultPathPolicyVariables,
                            new List<ApiClientPathPolicyMethod>());
                        pathPolicies.Add(path, policy);
                    }

                    var httpMethod = MethodToHttpMethod(method);
                    if (policy.PathPolicyMethods.Any(x => x.HttpMethod == httpMethod))
                    {
                        continue;
                    }

                    if (controllerPublicByDefault != null)
                    {
                        policy.PathPolicyMethods.Add(new ApiClientPathPolicyMethod(httpMethod, new List<ApiClientRequirement>(), true));
                    }
                    else
                    {
                        var requirements = controllerDefaultRequiredPermissions
                            .Where(x => x.Method == httpMethod || x.Method == null)
                            .Select(x => new ApiClientRequirement(x.PermissionPath, x.Permission ?? HttpMethodToPermissionType(httpMethod)))
                            .ToList();
                        policy.PathPolicyMethods.Add(new ApiClientPathPolicyMethod(httpMethod, requirements, false));
                    }
                }
            }

            var policies = pathPolicies.Values.ToList();
            if (optionsPoliciesPublicByDefault)
            {
                foreach (ApiClientPathPolicy policy in policies)
                {
                    if (policy.PathPolicyMethods.Any(x => x.HttpMethod == ApiClientHttpMethod.OPTIONS))
                    {
                        continue;
                    }

                    policy.PathPolicyMethods.Add(new ApiClientPathPolicyMethod(ApiClientHttpMethod.OPTIONS,
                        new List<ApiClientRequirement>(), true));
                }
            }

            return policies;
        }

        private static ApiClientHttpMethod MethodToHttpMethod(MethodInfo method)
        {
            if (method.GetCustomAttribute<HttpGetAttribute>() != null)
            {
                return ApiClientHttpMethod.GET;
            }

            if (method.GetCustomAttribute<HttpPutAttribute>() != null)
            {
                return ApiClientHttpMethod.PUT;
            }

            if (method.GetCustomAttribute<HttpPostAttribute>() != null)
            {
                return ApiClientHttpMethod.POST;
            }

            if (method.GetCustomAttribute<HttpDeleteAttribute>() != null)
            {
                return ApiClientHttpMethod.DELETE;
            }

            if (method.GetCustomAttribute<HttpPatchAttribute>() != null)
            {
                return ApiClientHttpMethod.PATCH;
            }

            if (method.GetCustomAttribute<HttpOptionsAttribute>() != null)
            {
                return ApiClientHttpMethod.OPTIONS;
            }

            if (method.GetCustomAttribute<HttpHeadAttribute>() != null)
            {
                return ApiClientHttpMethod.OPTIONS;
            }

            throw new PathPolicyBuilderException($"Couldn't determine HTTP method for {method.Name}.");
        }

        public static ApiClientPermission HttpMethodToPermissionType(ApiClientHttpMethod method)
        {
            return method switch
            {
                ApiClientHttpMethod.GET => ApiClientPermission.Read,
                ApiClientHttpMethod.PUT => ApiClientPermission.Write,
                ApiClientHttpMethod.POST => ApiClientPermission.Write,
                ApiClientHttpMethod.DELETE => ApiClientPermission.Write,
                ApiClientHttpMethod.OPTIONS => ApiClientPermission.Read,
                ApiClientHttpMethod.HEAD => ApiClientPermission.Read,
                ApiClientHttpMethod.CONNECT => ApiClientPermission.Write,
                ApiClientHttpMethod.TRACE => ApiClientPermission.Write,
                ApiClientHttpMethod.PATCH => ApiClientPermission.Write,
                ApiClientHttpMethod.ANY => ApiClientPermission.Write,
                _ => throw new PathPolicyBuilderException($"Method {method} not supported.")
            };
        }
    }
}
