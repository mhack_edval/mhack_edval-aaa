﻿using System;
using Edval.OS.AAA.Client.Model;

namespace Edval.OS.AAA.Tools.Permissions
{
    [AttributeUsage(validOn: AttributeTargets.Class, AllowMultiple = true)]
    public class ControllerDefaultRequiredPermissionAttribute : Attribute
    {
        public string PermissionPath { get; }
        public ApiClientHttpMethod? Method { get; }
        public ApiClientPermission? Permission { get; }

        /// <summary>
        /// Sets the default permissions required for each action in this controller. If they have an action specific attribute these won't apply.
        /// </summary>
        /// <param name="permissionPath">The required permission.</param>
        /// <param name="method">Not setting this will make it apply to all methods.</param>
        /// <param name="permission">Not setting the permission will default to the default for that http method type.</param>
        public ControllerDefaultRequiredPermissionAttribute(string permissionPath, ApiClientHttpMethod method = ApiClientHttpMethod.None, ApiClientPermission permission = ApiClientPermission.None)
        {
            PermissionPath = permissionPath;
            Method = method == ApiClientHttpMethod.None ? (ApiClientHttpMethod?) null : method;
            Permission = permission == ApiClientPermission.None ? (ApiClientPermission?)null : permission;
        }
    }
}
