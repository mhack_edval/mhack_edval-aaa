﻿using System;

namespace Edval.OS.AAA.Tools.Permissions
{
    /// <summary>
    /// Sets this action permission to public.
    /// </summary>
    [AttributeUsage(validOn: AttributeTargets.Method, AllowMultiple = false)]
    public class ActionPublicAttribute : Attribute
    {
        /// <summary>
        /// Sets this action permission to public.
        /// </summary>
        public ActionPublicAttribute()
        {
            
        }
    }
}
