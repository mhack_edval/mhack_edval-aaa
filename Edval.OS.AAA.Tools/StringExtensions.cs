﻿using System.Diagnostics.CodeAnalysis;

namespace Edval.OS.AAA.Tools
{
    public static class StringExtensions
    {
        internal static bool IsNullOrEmpty([NotNullWhen(false)]this string s)
        {
            return string.IsNullOrEmpty(s);
        }
    }
}
