﻿using System.Linq;
using Edval.OS.AAA.Tools.Middleware;
using Microsoft.AspNetCore.Http;

namespace Edval.OS.AAA.Tools
{
    public static class RequestExtensions
    {
        public static string? GetTenantId(this HttpRequest request) => 
            request.HttpContext.User?.Claims.SingleOrDefault(x => x.Type == AuthMiddlewareClaimsHelper.TenantId)?.Value;

        public static string? GetUserId(this HttpRequest request) => 
            request.HttpContext.User?.Claims.SingleOrDefault(x => x.Type == AuthMiddlewareClaimsHelper.UserId)?.Value;
    }
}