﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using System.Threading.Tasks;
using Edval.OS.AAA.Client.Api;
using Edval.OS.AAA.Client.Client;
using Edval.OS.AAA.Client.Model;
using Edval.OS.AAA.Tools.Permissions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Edval.OS.AAA.Tools
{
    public static class AAAExtensions
    {
        public static IWebHost RegisterPathPolicies(this IWebHost webHost, IAAAConfiguration config = null)
        {
            config ??= webHost.Services.GetRequiredService<IAAAConfiguration>();
            PolicyApi policyApi = new PolicyApi(config.AuthServiceUrl) { ExceptionFactory = null };
            policyApi.Configuration.ApiKey.Add("api-key", config.ApiKey);

            var policies = PathPolicyBuilder.GatherAndBuild(config.Assembly, config.Version, config.RoutePathPrefix,
                config.OptionsPoliciesPublicByDefault);

            List<Task<ApiResponse<object>>> tasks = new List<Task<ApiResponse<object>>>();
            foreach (ApiClientPathPolicy pathPolicy in policies)
            {
                tasks.Add(policyApi.PutPolicyPathWithHttpInfoAsync(pathPolicy.Path, pathPolicy));
            }

            Task.WhenAll(tasks).Wait();
            foreach (Task<ApiResponse<object>> task in tasks)
            {
                if (task.Result.StatusCode < 200 || task.Result.StatusCode >= 300)
                {
                    throw new Exception($"Failed calling AAA. StatusCode: {task.Result.StatusCode}\n\n" +
                                        $"Data: {JsonSerializer.Serialize(task.Result.Data)}");
                }
            }

            return webHost;
        }
    }
}
