﻿FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build

# Add JRE for build
RUN apt-get update \
  && apt-get install -y software-properties-common \
  && apt-add-repository 'deb http://security.debian.org/debian-security stretch/updates main' \
  && apt-get update \
  && apt-get install -y openjdk-11-jre-headless \
  && rm -rf /var/lib/apt/lists/*

# Build cache - Copy csproj and restore as distinct layers
WORKDIR /src
COPY *.sln .
COPY Edval.OS.AAA/Edval.OS.AAA.csproj Edval.OS.AAA/
COPY Edval.OS.AAA.Tests/Edval.OS.AAA.Tests.csproj Edval.OS.AAA.Tests/
COPY Edval.OS.AAA.Tools/Edval.OS.AAA.Tools.csproj Edval.OS.AAA.Tools/
RUN dotnet restore

# Copy everything else and test
COPY . .
RUN dotnet test -c RELEASE

# Publish
WORKDIR /src/Edval.OS.AAA
RUN dotnet publish -c RELEASE -o /publish

# Runtime image
FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /publish
COPY --from=build /publish .
ENTRYPOINT ["dotnet", "Edval.OS.AAA.dll"]
