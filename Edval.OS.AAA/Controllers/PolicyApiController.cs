﻿using System;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using Edval.OS.AAA.Database;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Edval.OS.AAA.Controllers
{
    public partial class PolicyApiController
    {
        private readonly AuthRepo _repo;
        private readonly ILogger<PolicyApiController> _logger;

        public PolicyApiController(AuthRepo repo, ILogger<PolicyApiController> logger)
        {
            _repo = repo;
            _logger = logger;
        }
        
        private async Task<IActionResult> GetPolicyPathInner(string id)
        {
            id = HttpUtility.UrlDecode(id);
            PathPolicy? result = await _repo.GetPathPolicy(id);
            if (result == null)
            {
                return NotFound();
            }

            (bool valid, string? reason) = result.Validate();

            if (!valid)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, "Failed validating policy: " + reason);
            }
            
            return Ok(result);
        }

        private async Task<IActionResult> GetPolicyUserInner(string id)
        {
            id = HttpUtility.UrlDecode(id);
            UserPolicy? result = await _repo.GetUserPolicy(id);
            if (result == null)
            {
                return NotFound();
            }

            (bool valid, string? reason) = result.Validate();

            if (!valid)
            {
                return StatusCode((int) HttpStatusCode.InternalServerError, "Failed validating policy: " + reason);
            }
            
            return Ok(result);
        }

        private async Task<IActionResult> PutPolicyPathInner(string id, ApiPathPolicy apiPathPolicy)
        {
            id = HttpUtility.UrlDecode(id);
            PathPolicy pathPolicy = apiPathPolicy.ToPathPolicy();
            if (id != pathPolicy.PathParts.ComposePath())
            {
                pathPolicy.Path = PathPart.DecomposePath(id).Select(x => x.Value).ToList();
            }
            
            (bool valid, var reason) = pathPolicy.Validate();

            if (!valid)
            {
                return BadRequest("Invalid path policy: " + reason);
            }

            await _repo.SetPathPolicy(pathPolicy);
            
            return Ok();
        }

        private async Task<IActionResult> PutPolicyUserInner(string id, ApiUserPolicy apiUserPolicy)
        {
            id = HttpUtility.UrlDecode(id);
            UserPolicy userPolicy;

            try
            {
                userPolicy = apiUserPolicy.ToUserPolicy();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed converting ApiUserPolicy to UserPolicy.\nPolicy: " + JsonSerializer.Serialize(apiUserPolicy));
                return BadRequest("Failed converting ApiUserPolicy to UserPolicy");
            }

            if (id != userPolicy.UserId)
            {
                userPolicy.UserId = id;
            }
            
            (bool valid, var reason) = userPolicy.Validate();

            if (!valid)
            {
                return BadRequest("Invalid user policy: " + reason);
            }

            await _repo.SetUserPolicy(userPolicy);
            return Ok();
        }
    }
}
