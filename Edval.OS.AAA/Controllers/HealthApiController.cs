﻿using System.Threading.Tasks;
using Edval.OS.AAA.Models;
using Microsoft.AspNetCore.Mvc;

namespace Edval.OS.AAA.Controllers
{
    public partial class HealthApiController
    {
        private async Task<IActionResult> GetHealthInner()
        {
            return Ok(new ApiHealthStatus() { Healthy = true });
        }
    }
}
