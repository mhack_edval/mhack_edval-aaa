﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Amazon.CognitoIdentityProvider.Model;
using Edval.OS.AAA.Authentication;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Configuration;
using Edval.OS.AAA.Database;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.JWT;
using Edval.OS.AAA.JWT.PublicKeys;
using Edval.OS.AAA.Models.User;
using Edval.OS.AAA.Services;
using JsonWebToken;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiAuthorisationRequest = Edval.OS.AAA.Models.ApiAuthorisationRequest;
using ApiUserPolicy = Edval.OS.AAA.Models.ApiUserPolicy;
using ApiUserPolicyPath = Edval.OS.AAA.Models.ApiUserPolicyPath;

namespace Edval.OS.AAA.Controllers
{
    public partial class AuthApiController
    {
        private readonly RequestAuthoriser<CognitoPublicKeys> _requestAuthoriser;
        private readonly CognitoInterface _cognitoInterface;
        private readonly CognitoConfiguration _cognitoConfiguration;
        private readonly AuthRepo _authRepo;
        private readonly AuthCookieConfiguration _authCookieConfiguration;
        private readonly ILogger<AuthApiController> _logger;

        public AuthApiController(RequestAuthoriser<CognitoPublicKeys> requestAuthoriser,
            CognitoInterface cognitoInterface, CognitoConfiguration cognitoConfiguration,
            AuthRepo authRepo, AuthCookieConfiguration authCookieConfiguration, ILogger<AuthApiController> logger)
        {
            _requestAuthoriser = requestAuthoriser;
            _cognitoInterface = cognitoInterface;
            _cognitoConfiguration = cognitoConfiguration;
            _authRepo = authRepo;
            _authCookieConfiguration = authCookieConfiguration;
            _logger = logger;
        }

        private CookieOptions AccessTokenCookieOptions => new CookieOptions()
        {
            Expires = DateTimeOffset.UtcNow + TimeSpan.FromHours(_authCookieConfiguration.ExpiryInHours),
            HttpOnly = _authCookieConfiguration.HttpOnly,
            IsEssential = true,
            SameSite = Enum.Parse<SameSiteMode>(_authCookieConfiguration.SameSiteMode),
            Secure = _authCookieConfiguration.Secure,
            Domain = _authCookieConfiguration.Domain
        };

        private void SetCookie(string accessToken)
        {
            HttpContext.Response.Cookies.Append("EdvalOS_Auth", accessToken, AccessTokenCookieOptions);
        }

        private async Task<IActionResult> GetAuthenticateInner(string username, string password)
        {
            // TODO IP bans, repeat attempts etc.

            Response.ContentType = "application/json";
            if (username.IsNullOrEmpty())
            {
                return Unauthorized(new ApiAuthenticateResponse() { Success = false, Details = "Missing username" });
            }

            if (password.IsNullOrEmpty())
            {
                return Unauthorized(new ApiAuthenticateResponse() { Success = false, Details = "Missing password" });
            }

            if (!username.Contains("@"))
            {
                return Unauthorized(new ApiAuthenticateResponse() { Success = false, Details = "Username must be an email" });
            }

            if (password.Length < 6)
            {
                return Unauthorized(new ApiAuthenticateResponse() { Success = false, Details = "Password must be 6 or more characters" });
            }

            CognitoInterface.LoginResult loginResult = await _cognitoInterface.Login(username, password);

            if (!loginResult.UserLoggedIn)
            {
                return Unauthorized(new ApiAuthenticateResponse()
                {
                    Success = false,
                    Details = loginResult.FailAdvice
                });
            }

            if (loginResult.AccessToken.IsNullOrEmpty())
            {
                throw new Exception("AccessToken was null or empty but the user was successfully logged in???");
            }

            JwtHandler handler = new JwtHandler(_cognitoConfiguration.ValidTokenIssuer,
                await PublicKeyCache<CognitoPublicKeys>.GetKeys(_cognitoConfiguration.PublicKeyUrl));
            handler.TryValidateToken(loginResult.AccessToken, SignatureAlgorithm.RsaSha256, out Jwt token);

            if (token == null || token.Subject.IsNullOrEmpty())
            {
                throw new Exception("Token was null or subject was null or empty.");
            }

            UserPolicy? userPolicy = await _authRepo.GetUserPolicy(token.Subject);
            if (userPolicy == null)
            {
                await _cognitoInterface.DeleteUser(username);
                throw new Exception("Couldn't find user policy!");
            }

            SetCookie(loginResult.AccessToken);
            Console.WriteLine($"User logged in {userPolicy.UserId} {userPolicy.Name}. User cookie set - Domain: " + _authCookieConfiguration.Domain);
            bool internalUser = userPolicy.AllowedPaths.TryGetChild("internal", out UserPolicyPath internalPath) &&
                                (int)internalPath.GrantedPermission >= (int)Permission.Read;
            return Ok(new ApiAuthenticateResponse()
            {
                Success = true,
                UserId = userPolicy.UserId,
                TenantId = userPolicy.TenantId,
                Internal = internalUser
            });
        }

        private async Task<IActionResult> AuthoriseInner(ApiAuthorisationRequest apiAuthorisationRequest)
        {
            if (apiAuthorisationRequest == null)
            {
                return BadRequest(new ApiAuthorisationResponse()
                {
                    Success = false,
                    Details = "Missing body."
                });
            }

            Console.WriteLine("Authorising: " + apiAuthorisationRequest.Url);

            string? authToken = apiAuthorisationRequest.TryGetAuthToken();
            Request requestForAuth = new Request(apiAuthorisationRequest.Url, authToken ?? "",
                apiAuthorisationRequest.GetHttpMethod(),
                apiAuthorisationRequest.Headers?.ToDictionary(x => x.Key, x => x.Value?.ToString()),
                apiAuthorisationRequest.Queries?.ToDictionary(x => x.Key, x => x.Value?.ToString()),
                apiAuthorisationRequest.PathParameters?.ToDictionary(x => x.Key, x => x.Value?.ToString()));

            Stopwatch sw = Stopwatch.StartNew();
            (bool success, Jwt? jwt, UserPolicy? userPolicy) = await _requestAuthoriser.IsRequestAuthorised(requestForAuth);
            sw.Stop();
            Console.WriteLine($"Auth duration: {sw.Elapsed.TotalMilliseconds:F2}ms");

            return Ok(new ApiAuthorisationResponse()
            {
                Success = success,
                Details = success ? "Success" : "Fail",
                TenantId = userPolicy?.TenantId,
                UserId = userPolicy?.UserId
            });
        }

        private async Task<IActionResult> CreateUserInner(ApiCreateUserRequest apiCreateUserRequest)
        {
            if (apiCreateUserRequest.UserEmail.IsNullOrEmpty() || !apiCreateUserRequest.UserEmail.Contains('@'))
            {
                return BadRequest(new ApiCreateUserResponse() { Success = false, Details = "No email provided or email doesn't contain '@' symbol." });
            }

            // sign user up to Cognito
            bool signUpSuccess;
            string? userId;
            try
            {
                (signUpSuccess, _, _, userId) = await _cognitoInterface.SignUp(apiCreateUserRequest.UserEmail, apiCreateUserRequest.UserEmail);
            }
            catch (UsernameExistsException)
            {
                return BadRequest(new ApiCreateUserResponse() { Success = false, Details = "Username already exists." });
            }

            if (!signUpSuccess || userId.IsNullOrEmpty())
            {
                return BadRequest(new ApiCreateUserResponse() { Success = false, Details = "Failed creating user." });
            }

            try
            {
                // set their password so they don't have to change on first login
                string userPassword = apiCreateUserRequest.Password.IsNullOrEmpty()
                    ? PasswordGenerator.RandomPassword()
                    : apiCreateUserRequest.Password;
                bool setPassword =
                    await _cognitoInterface.ChangeUserPassword(userPassword, apiCreateUserRequest.UserEmail);
                if (!setPassword)
                {
                    await _cognitoInterface.DeleteUser(apiCreateUserRequest.UserEmail);
                    return BadRequest(new ApiCreateUserResponse()
                    { Success = false, Details = "Failed setting user password. Does it have letters, numbers and symbols?" });
                }

                if (userPassword.IsNullOrEmpty())
                {
                    throw new Exception(nameof(userPassword) + " was null.");
                }

                List<UserPolicyPath> userPolicyPaths = new List<UserPolicyPath>();
                if (apiCreateUserRequest.ReadScopes != null)
                {
                    AppendUserPolicyPaths(userPolicyPaths, apiCreateUserRequest.ReadScopes, Permission.Read);
                }

                if (apiCreateUserRequest.WriteScopes != null)
                {
                    AppendUserPolicyPaths(userPolicyPaths, apiCreateUserRequest.WriteScopes, Permission.Write);
                }

                if (apiCreateUserRequest.InternalUser == true)
                {
                    AppendUserPolicyPaths(userPolicyPaths, new List<string>() { "internal" }, Permission.Write);
                }

                // add user policy
                string tenantId = apiCreateUserRequest.TenantId.IsNullOrEmpty()
                    ? Guid.NewGuid().ToString()
                    : apiCreateUserRequest.TenantId;
                UserPolicy policy = new UserPolicy("1.0.0", apiCreateUserRequest.UserEmail, userId, tenantId,
                    userPolicyPaths);
                await _authRepo.SetUserPolicy(policy);

                return Ok(new ApiCreateUserResponse()
                {
                    Success = true,
                    UserId = userId,
                    UserPassword = userPassword
                });
            }
            catch (Exception e)
            {
                await Console.Error.WriteLineAsync("Failed creating user: " + e);
                await _cognitoInterface.DeleteUser(apiCreateUserRequest.UserEmail);
                throw;
            }
        }

        public static void AppendUserPolicyPaths(List<UserPolicyPath> userPolicyPaths, List<string> composedPaths,
            Permission grantedPermission)
        {
            foreach (string readScope in composedPaths)
            {
                List<PathPart> pathParts = PathPart.DecomposePath(readScope);
                UserPolicyPath? parent = null;
                for (int i = 0; i < pathParts.Count; i++)
                {
                    PathPart pathPart = pathParts[i];
                    bool firstIteration = parent == null;
                    bool lastIteration = i == pathParts.Count - 1;

                    if (firstIteration)
                    {
                        if (userPolicyPaths.TryGetChild(pathPart.Value, out UserPolicyPath potentialParent))
                        {
                            parent = potentialParent;
                        }
                        else
                        {
                            parent = new UserPolicyPath(pathPart, Permission.None);
                            userPolicyPaths.Add(parent);
                        }
                    }
                    else
                    {
                        UserPolicyPath? newParent = null;
                        Debug.Assert(parent != null, nameof(parent) + " != null");
                        if (parent.Children?.TryGetChild(pathPart, out newParent) == true) // already present
                        {
                            Debug.Assert(newParent != null, nameof(newParent) + " != null");
                            parent = newParent;
                        }
                        else // add new child
                        {
                            parent.Children ??= new List<UserPolicyPath>();
                            UserPolicyPath? newPolicy = new UserPolicyPath(pathPart, Permission.None);
                            parent.Children.Add(newPolicy);
                            parent = newPolicy;
                        }
                    }

                    if (lastIteration) // set permission on farthest node
                    {
                        Debug.Assert(parent != null, nameof(parent) + " != null");
                        parent.GrantedPermission = grantedPermission;
                    }
                }
            }
        }

        private async Task<IActionResult> DeleteUserIdInner(string username)
        {
            if (username.IsNullOrEmpty())
            {
                return BadRequest();
            }

            var userDetails = await _cognitoInterface.GetUser(username);
            if (userDetails == null || userDetails.UserId.IsNullOrEmpty())
            {
                return NotFound();
            }

            await _cognitoInterface.DeleteUser(username);
            await _authRepo.DeleteUserPolicy(userDetails.UserId);
            return Ok();
        }

        private async Task<IActionResult> GetUserIdInner(string username)
        {
            if (username.IsNullOrEmpty())
            {
                return BadRequest();
            }

            var userDetails = await _cognitoInterface.GetUser(username);
            if (userDetails == null)
            {
                return NotFound();
            }

            var userPolicy = await _authRepo.GetUserPolicy(userDetails.UserId);
            if (userPolicy == null)
            {
                return NotFound();
            }

            bool internalUser = userPolicy.AllowedPaths.TryGetChild("internal", out UserPolicyPath internalPath) &&
                                internalPath?.GrantedPermission == Permission.All;

            var apiUserPolicy = new ApiUserPolicy()
            {
                Name = userPolicy.Name,
                UserId = userPolicy.UserId,
                TenantId = userPolicy.TenantId,
                Version = userPolicy.Version,
                AllowedPaths = ToApiUserPolicyPaths(userPolicy.AllowedPaths)
            };

            ApiUser returnUser = new ApiUser()
            {
                Id = userPolicy.UserId,
                TenantId = userPolicy.TenantId,
                InternalUser = internalUser,
                Policy = apiUserPolicy
            };

            return Ok(returnUser);
        }

        private List<ApiUserPolicyPath>? ToApiUserPolicyPaths(List<UserPolicyPath>? allowedPaths)
        {
            List<ApiUserPolicyPath> converted = new List<ApiUserPolicyPath>();
            if (allowedPaths == null)
            {
                return null;
            }

            foreach (UserPolicyPath userPolicyPath in allowedPaths)
            {
                converted.Add(new ApiUserPolicyPath()
                {
                    GrantedPermission = ConvertPermission(userPolicyPath.GrantedPermission),
                    Path = userPolicyPath.Path,
                    Children = ToApiUserPolicyPaths(userPolicyPath.Children),
                });
            }

            return converted;
        }

        private ApiPermission ConvertPermission(Permission permission)
        {
            return permission switch
            {
                Permission.None => ApiPermission.NoneEnum,
                Permission.All => ApiPermission.AllEnum,
                Permission.Read => ApiPermission.ReadEnum,
                Permission.Write => ApiPermission.WriteEnum,
                _ => throw new ArgumentOutOfRangeException(nameof(permission))
            };
        }

        private async Task<IActionResult> PostUserUsernamePasswordInner(string username, ApiSetUserPasswordRequest apiSetUserPasswordRequest)
        {
            if (username.IsNullOrEmpty() || !username.Contains('@'))
            {
                return BadRequest(new ApiSetUserPasswordResponse { Success = false, Details = "No email provided or email doesn't contain '@' symbol." });
            }

            if (apiSetUserPasswordRequest.Password.IsNullOrEmpty())
            {
                return BadRequest(new ApiSetUserPasswordResponse { Success = false, Details = "Password was empty." });
            }

            _logger.LogInformation($"Changing user {username} password.");
            var success = await _cognitoInterface.ChangeUserPassword(apiSetUserPasswordRequest.Password, username);

            if (!success)
            {
                return BadRequest(new ApiSetUserPasswordResponse { Success = false, Details = "Failed setting new password." });
            }

            return Ok(new ApiSetUserPasswordResponse{Success = true});
        }
    }
}
