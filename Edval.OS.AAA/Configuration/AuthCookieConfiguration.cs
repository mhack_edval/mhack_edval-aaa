﻿namespace Edval.OS.AAA.Configuration
{
    public class AuthCookieConfiguration
    {
        public string Domain { get; set; }
        public string SameSiteMode { get; set; }
        public bool HttpOnly { get; set; }
        public bool Secure { get; set; }
        public int ExpiryInHours { get; set; }
    }
}
