﻿using System;
using System.Collections;
using Edval.OS.AAA.Extensions;
using Microsoft.Extensions.Configuration;

namespace Edval.OS.AAA.Configuration
{
    public class PosixEnvironmentVariableConfigurationProvider : ConfigurationProvider
    {
        private readonly PosixEnvironmentVariableConfigurationSource _source;

        public PosixEnvironmentVariableConfigurationProvider(PosixEnvironmentVariableConfigurationSource source)
        {
            _source = source;
        }

        public override void Load()
        {
            var environmentVariables = Environment.GetEnvironmentVariables();
            foreach (DictionaryEntry? environmentVariable in environmentVariables)
            {
                if (environmentVariable == null)
                {
                    continue;
                }

                string? key = environmentVariable.Value.Key?.ToString();
                if (key.IsNullOrEmpty())
                {
                    continue;
                }

                string? value = environmentVariable.Value.Value?.ToString();
                if (value.IsNullOrEmpty())
                {
                    continue;
                }

                key = key.ToLowerInvariant().Replace("_", ":");
                Set(key, value);
            }
        }
    }
}