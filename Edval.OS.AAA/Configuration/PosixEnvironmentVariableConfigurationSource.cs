﻿using Microsoft.Extensions.Configuration;

namespace Edval.OS.AAA.Configuration
{
    public class PosixEnvironmentVariableConfigurationSource : IConfigurationSource
    {
        public PosixEnvironmentVariableConfigurationSource(PosixEnvironmentVariableConfigurationOptions configurationOptions)
        {
        }

        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new PosixEnvironmentVariableConfigurationProvider(this);
        }
    }
}
