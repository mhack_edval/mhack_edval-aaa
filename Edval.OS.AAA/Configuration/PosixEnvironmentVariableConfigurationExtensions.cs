﻿using System;
using Microsoft.Extensions.Configuration;

namespace Edval.OS.AAA.Configuration
{
    public static class PosixEnvironmentVariableConfigurationExtensions
    {
        public static IConfigurationBuilder AddPosixEnvironmentVariable(this IConfigurationBuilder configuration, Action<PosixEnvironmentVariableConfigurationOptions> options)
        {
            _ = options ?? throw new ArgumentNullException(nameof(options));
            PosixEnvironmentVariableConfigurationOptions? optionsInstance = new PosixEnvironmentVariableConfigurationOptions();
            options(optionsInstance);
            configuration.Add(new PosixEnvironmentVariableConfigurationSource(optionsInstance));
            return configuration;
        }
    }
}
