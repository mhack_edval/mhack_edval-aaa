﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Edval.OS.AAA.Middleware
{
    public class NoCacheByDefaultMiddleware
    {
        private readonly RequestDelegate _next;

        public NoCacheByDefaultMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            context.Response.SetDontCacheHeader();
            await _next(context);
        }
    }
}
