﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Edval.OS.AAA.Middleware
{
    public static class NoCacheByDefaultMiddlewareExtensions
    {
        public static IApplicationBuilder UseNoCacheByDefault(this IApplicationBuilder app)
        {
            app.UseMiddleware<NoCacheByDefaultMiddleware>();
            return app;
        }

        public static HttpResponse SetCacheControlHeader(this HttpResponse response, string value)
        {
            response.Headers["Cache-Control"] = value;
            return response;
        }

        public static HttpResponse SetSurrogateControlHeader(this HttpResponse response, string value)
        {
            response.Headers["Surrogate-Control"] = value;
            return response;
        }

        public static HttpResponse SetDontCacheHeader(this HttpResponse response)
        {
            response.Headers["Cache-Control"] = "private, no-store";
            return response;
        }
    }
}
