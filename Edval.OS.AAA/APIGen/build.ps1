if (Test-Path -Path '.\client' -PathType Container)
{
	rm -r -fo ".\client"
	"Client folder deleted.`n`n"
}

"`n`n==================================================================================="
"======================================  API  ======================================"
"==================================================================================="

"`n-------------------------------------------------------------------------------"
"C# Client"
"-------------------------------------------------------------------------------`n"
java -jar ".\tools\open-api-generator-cli.jar" generate -i "..\Edval.OS.AAA.AuthService.yaml" -g csharp -o ".\client\DemoAPI\csharp_api_client" --package-name "DemoAPI.Client" --model-name-prefix "ApiClient" --enable-post-process-file -t ".\templates\csharp" 

"`n`n-------------------------------------------------------------------------------"
"TypeScript Client"
"-------------------------------------------------------------------------------`n"
java -jar ".\tools\open-api-generator-cli.jar" generate -i "..\DemoAPI.yaml" -g typescript-axios -o ".\client\DemoAPI\typescript_api_client" --package-name "DemoAPI.Client" --model-name-prefix "ApiClient"

"`n`n==================================================================================="
"======================================  Build  ===================================="
"==================================================================================="

cd .\client\DemoAPI\csharp_api_client
.\build.bat

cd ..
cd ..
cd ..

rm -r -fo ".\nuget.exe"

Read-Host