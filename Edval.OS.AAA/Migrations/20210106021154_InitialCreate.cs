﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Edval.OS.AAA.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PathPolicies",
                columns: table => new
                {
                    Key = table.Column<string>(type: "text", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPolicies", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "UserPolicies",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    TenantId = table.Column<string>(type: "text", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPolicies", x => new { x.UserId, x.TenantId });
                });

            migrationBuilder.CreateTable(
                name: "PathPolicyMethod",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    HttpMethod = table.Column<int>(type: "integer", nullable: false),
                    AllowPublicAccess = table.Column<bool>(type: "boolean", nullable: false),
                    PathPolicyKey = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPolicyMethod", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PathPolicyMethod_PathPolicies_PathPolicyKey",
                        column: x => x.PathPolicyKey,
                        principalTable: "PathPolicies",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PathPolicyVariable",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PolicyKey = table.Column<string>(type: "text", nullable: true),
                    Location = table.Column<int>(type: "integer", nullable: false),
                    RequestKey = table.Column<string>(type: "text", nullable: true),
                    PathPolicyKey = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPolicyVariable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PathPolicyVariable_PathPolicies_PathPolicyKey",
                        column: x => x.PathPolicyKey,
                        principalTable: "PathPolicies",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPolicyPath",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GrantedPermission = table.Column<int>(type: "integer", nullable: false),
                    UserPolicyPathId = table.Column<Guid>(type: "uuid", nullable: true),
                    UserPolicyTenantId = table.Column<string>(type: "text", nullable: true),
                    UserPolicyUserId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPolicyPath", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId_UserPolicyTena~",
                        columns: x => new { x.UserPolicyUserId, x.UserPolicyTenantId },
                        principalTable: "UserPolicies",
                        principalColumns: new[] { "UserId", "TenantId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPolicyPath_UserPolicyPath_UserPolicyPathId",
                        column: x => x.UserPolicyPathId,
                        principalTable: "UserPolicyPath",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Requirement",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RequiredPermission = table.Column<int>(type: "integer", nullable: false),
                    PathPolicyMethodId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requirement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Requirement_PathPolicyMethod_PathPolicyMethodId",
                        column: x => x.PathPolicyMethodId,
                        principalTable: "PathPolicyMethod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PathPart",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true),
                    PathPolicyKey = table.Column<string>(type: "text", nullable: true),
                    RequirementId = table.Column<Guid>(type: "uuid", nullable: true),
                    UserPolicyPathId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PathPart_PathPolicies_PathPolicyKey",
                        column: x => x.PathPolicyKey,
                        principalTable: "PathPolicies",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PathPart_Requirement_RequirementId",
                        column: x => x.RequirementId,
                        principalTable: "Requirement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PathPart_UserPolicyPath_UserPolicyPathId",
                        column: x => x.UserPolicyPathId,
                        principalTable: "UserPolicyPath",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PathPart_PathPolicyKey",
                table: "PathPart",
                column: "PathPolicyKey");

            migrationBuilder.CreateIndex(
                name: "IX_PathPart_RequirementId",
                table: "PathPart",
                column: "RequirementId");

            migrationBuilder.CreateIndex(
                name: "IX_PathPart_UserPolicyPathId",
                table: "PathPart",
                column: "UserPolicyPathId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PathPolicyMethod_PathPolicyKey",
                table: "PathPolicyMethod",
                column: "PathPolicyKey");

            migrationBuilder.CreateIndex(
                name: "IX_PathPolicyVariable_PathPolicyKey",
                table: "PathPolicyVariable",
                column: "PathPolicyKey");

            migrationBuilder.CreateIndex(
                name: "IX_Requirement_PathPolicyMethodId",
                table: "Requirement",
                column: "PathPolicyMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicyPath_UserPolicyPathId",
                table: "UserPolicyPath",
                column: "UserPolicyPathId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicyPath_UserPolicyUserId_UserPolicyTenantId",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId", "UserPolicyTenantId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PathPart");

            migrationBuilder.DropTable(
                name: "PathPolicyVariable");

            migrationBuilder.DropTable(
                name: "Requirement");

            migrationBuilder.DropTable(
                name: "UserPolicyPath");

            migrationBuilder.DropTable(
                name: "PathPolicyMethod");

            migrationBuilder.DropTable(
                name: "UserPolicies");

            migrationBuilder.DropTable(
                name: "PathPolicies");
        }
    }
}
