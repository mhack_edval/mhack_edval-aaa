﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Edval.OS.AAA.Migrations
{
    public partial class AddFlattenedPaths : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserPolicyTenantId1",
                table: "UserPolicyPath",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserPolicyUserId1",
                table: "UserPolicyPath",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicyPath_UserPolicyUserId1_UserPolicyTenantId1",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId1", "UserPolicyTenantId1" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId1", "UserPolicyTenantId1" },
                principalTable: "UserPolicies",
                principalColumns: new[] { "UserId", "TenantId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                table: "UserPolicyPath");

            migrationBuilder.DropIndex(
                name: "IX_UserPolicyPath_UserPolicyUserId1_UserPolicyTenantId1",
                table: "UserPolicyPath");

            migrationBuilder.DropColumn(
                name: "UserPolicyTenantId1",
                table: "UserPolicyPath");

            migrationBuilder.DropColumn(
                name: "UserPolicyUserId1",
                table: "UserPolicyPath");
        }
    }
}
