﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Edval.OS.AAA.Migrations
{
    public partial class AddApiKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                table: "UserPolicyPath");

            migrationBuilder.CreateTable(
                name: "ApiKeys",
                columns: table => new
                {
                    Key = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiKeys", x => x.Key);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId1", "UserPolicyTenantId1" },
                principalTable: "UserPolicies",
                principalColumns: new[] { "UserId", "TenantId" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                table: "UserPolicyPath");

            migrationBuilder.DropTable(
                name: "ApiKeys");

            migrationBuilder.AddForeignKey(
                name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId1", "UserPolicyTenantId1" },
                principalTable: "UserPolicies",
                principalColumns: new[] { "UserId", "TenantId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
