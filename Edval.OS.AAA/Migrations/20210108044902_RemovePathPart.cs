﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Edval.OS.AAA.Migrations
{
    public partial class RemovePathPart : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PathPart");

            migrationBuilder.AddColumn<string>(
                name: "Path",
                table: "UserPolicyPath",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<List<string>>(
                name: "Path",
                table: "Requirement",
                type: "text[]",
                nullable: true);

            migrationBuilder.AddColumn<List<string>>(
                name: "Path",
                table: "PathPolicies",
                type: "text[]",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Path",
                table: "UserPolicyPath");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "Requirement");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "PathPolicies");

            migrationBuilder.CreateTable(
                name: "PathPart",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PathPolicyKey = table.Column<string>(type: "text", nullable: true),
                    RequirementId = table.Column<Guid>(type: "uuid", nullable: true),
                    UserPolicyPathId = table.Column<Guid>(type: "uuid", nullable: true),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PathPart_PathPolicies_PathPolicyKey",
                        column: x => x.PathPolicyKey,
                        principalTable: "PathPolicies",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PathPart_Requirement_RequirementId",
                        column: x => x.RequirementId,
                        principalTable: "Requirement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PathPart_UserPolicyPath_UserPolicyPathId",
                        column: x => x.UserPolicyPathId,
                        principalTable: "UserPolicyPath",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PathPart_PathPolicyKey",
                table: "PathPart",
                column: "PathPolicyKey");

            migrationBuilder.CreateIndex(
                name: "IX_PathPart_RequirementId",
                table: "PathPart",
                column: "RequirementId");

            migrationBuilder.CreateIndex(
                name: "IX_PathPart_UserPolicyPathId",
                table: "PathPart",
                column: "UserPolicyPathId",
                unique: true);
        }
    }
}
