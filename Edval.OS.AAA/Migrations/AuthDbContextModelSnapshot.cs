﻿// <auto-generated />

using System;
using System.Collections.Generic;
using Edval.OS.AAA.Database.PostGres;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Edval.OS.AAA.Migrations
{
    [DbContext(typeof(AuthDbContext))]
    partial class AuthDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("Edval.OS.AAA.AuthService.ApiKeys.ApiKey", b =>
                {
                    b.Property<string>("Key")
                        .HasColumnType("text");

                    b.Property<DateTime>("Created")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Owner")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<List<string>>("Roles")
                        .IsRequired()
                        .HasColumnType("text[]");

                    b.HasKey("Key");

                    b.ToTable("ApiKeys");
                });

            modelBuilder.Entity("Edval.OS.AAA.AuthService.Database.DbPathPolicy", b =>
                {
                    b.Property<string>("Key")
                        .HasColumnType("text");

                    b.Property<DateTime>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Policy")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<decimal>("Version")
                        .HasColumnType("numeric(20,0)");

                    b.HasKey("Key");

                    b.HasIndex("Key");

                    b.ToTable("PathPolicies");
                });

            modelBuilder.Entity("Edval.OS.AAA.AuthService.Database.DbUserPolicy", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("text");

                    b.Property<DateTime>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Policy")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<decimal>("Version")
                        .HasColumnType("numeric(20,0)");

                    b.HasKey("UserId");

                    b.HasIndex("UserId");

                    b.ToTable("UserPolicies");
                });
#pragma warning restore 612, 618
        }
    }
}
