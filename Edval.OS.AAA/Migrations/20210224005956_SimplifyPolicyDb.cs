﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Edval.OS.AAA.Migrations
{
    public partial class SimplifyPolicyDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PathPolicyVariable");

            migrationBuilder.DropTable(
                name: "Requirement");

            migrationBuilder.DropTable(
                name: "UserPolicyPath");

            migrationBuilder.DropTable(
                name: "PathPolicyMethod");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserPolicies",
                table: "UserPolicies");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "UserPolicies");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "UserPolicies");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "PathPolicies");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "PathPolicies");

            migrationBuilder.RenameColumn(
                name: "TenantId",
                table: "UserPolicies",
                newName: "Policy");

            migrationBuilder.AddColumn<string>(
                name: "Policy",
                table: "PathPolicies",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserPolicies",
                table: "UserPolicies",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicies_UserId",
                table: "UserPolicies",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PathPolicies_Key",
                table: "PathPolicies",
                column: "Key");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserPolicies",
                table: "UserPolicies");

            migrationBuilder.DropIndex(
                name: "IX_UserPolicies_UserId",
                table: "UserPolicies");

            migrationBuilder.DropIndex(
                name: "IX_PathPolicies_Key",
                table: "PathPolicies");

            migrationBuilder.DropColumn(
                name: "Policy",
                table: "PathPolicies");

            migrationBuilder.RenameColumn(
                name: "Policy",
                table: "UserPolicies",
                newName: "TenantId");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "UserPolicies",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Version",
                table: "UserPolicies",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string[]>(
                name: "Path",
                table: "PathPolicies",
                type: "text[]",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Version",
                table: "PathPolicies",
                type: "text",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserPolicies",
                table: "UserPolicies",
                columns: new[] { "UserId", "TenantId" });

            migrationBuilder.CreateTable(
                name: "PathPolicyMethod",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AllowPublicAccess = table.Column<bool>(type: "boolean", nullable: false),
                    HttpMethod = table.Column<int>(type: "integer", nullable: false),
                    PathPolicyKey = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPolicyMethod", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PathPolicyMethod_PathPolicies_PathPolicyKey",
                        column: x => x.PathPolicyKey,
                        principalTable: "PathPolicies",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PathPolicyVariable",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Location = table.Column<int>(type: "integer", nullable: false),
                    PathPolicyKey = table.Column<string>(type: "text", nullable: true),
                    PolicyKey = table.Column<string>(type: "text", nullable: true),
                    RequestKey = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PathPolicyVariable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PathPolicyVariable_PathPolicies_PathPolicyKey",
                        column: x => x.PathPolicyKey,
                        principalTable: "PathPolicies",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPolicyPath",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GrantedPermission = table.Column<int>(type: "integer", nullable: false),
                    Path = table.Column<string>(type: "text", nullable: true),
                    UserPolicyPathId = table.Column<Guid>(type: "uuid", nullable: true),
                    UserPolicyTenantId = table.Column<string>(type: "text", nullable: true),
                    UserPolicyTenantId1 = table.Column<string>(type: "text", nullable: true),
                    UserPolicyUserId = table.Column<string>(type: "text", nullable: true),
                    UserPolicyUserId1 = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPolicyPath", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId_UserPolicyTena~",
                        columns: x => new { x.UserPolicyUserId, x.UserPolicyTenantId },
                        principalTable: "UserPolicies",
                        principalColumns: new[] { "UserId", "TenantId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPolicyPath_UserPolicies_UserPolicyUserId1_UserPolicyTen~",
                        columns: x => new { x.UserPolicyUserId1, x.UserPolicyTenantId1 },
                        principalTable: "UserPolicies",
                        principalColumns: new[] { "UserId", "TenantId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPolicyPath_UserPolicyPath_UserPolicyPathId",
                        column: x => x.UserPolicyPathId,
                        principalTable: "UserPolicyPath",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Requirement",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Path = table.Column<string[]>(type: "text[]", nullable: true),
                    PathPolicyMethodId = table.Column<Guid>(type: "uuid", nullable: true),
                    RequiredPermission = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requirement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Requirement_PathPolicyMethod_PathPolicyMethodId",
                        column: x => x.PathPolicyMethodId,
                        principalTable: "PathPolicyMethod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PathPolicyMethod_PathPolicyKey",
                table: "PathPolicyMethod",
                column: "PathPolicyKey");

            migrationBuilder.CreateIndex(
                name: "IX_PathPolicyVariable_PathPolicyKey",
                table: "PathPolicyVariable",
                column: "PathPolicyKey");

            migrationBuilder.CreateIndex(
                name: "IX_Requirement_PathPolicyMethodId",
                table: "Requirement",
                column: "PathPolicyMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicyPath_UserPolicyPathId",
                table: "UserPolicyPath",
                column: "UserPolicyPathId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicyPath_UserPolicyUserId_UserPolicyTenantId",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId", "UserPolicyTenantId" });

            migrationBuilder.CreateIndex(
                name: "IX_UserPolicyPath_UserPolicyUserId1_UserPolicyTenantId1",
                table: "UserPolicyPath",
                columns: new[] { "UserPolicyUserId1", "UserPolicyTenantId1" });
        }
    }
}
