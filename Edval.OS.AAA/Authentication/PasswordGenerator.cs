﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Edval.OS.AAA.Extensions;

namespace Edval.OS.AAA.Authentication
{
    public static class PasswordGenerator
    {
        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();

        private static readonly FuzzyStringMatcher<bool> ForbiddenWordMatcher =
            new FuzzyStringMatcher<bool>(ProfanityList.List.ToDictionary(x => x, x => true), true, false, false);

        private static char[] AcceptablePunctuation { get; } = {
            '!',
            '@',
            '#',
            '$',
            '%',
            '&',
            '*',
            '-',
            '+',
            '?',
            '_',
            '='
        };

        /// <summary>
        /// Returns a cryptographically secure completely random password. Length: min 6, max 64.
        /// </summary>
        /// <param name="length">Min 6, max 64</param>
        /// <returns></returns>
        public static string RandomPassword(int length = 8, bool requireNumber = true, bool requireSymbol = true, bool requiresUppercaseAndLowercaseChar = true)
        {
            if (length > 64)
            {
                throw new ArgumentException(nameof(length) + " must be <= 64 chars.",  nameof(length));
            }

            if (length < 6)
            {
                throw new ArgumentException(nameof(length) + " must be >= 6 chars.",  nameof(length));
            }

            Span<byte> bytes = stackalloc byte[length];
            Span<byte> tempBytes = stackalloc byte[2];
            Span<int> changedIndexes = stackalloc int[4];

            while (true)
            {
                changedIndexes.Fill(-1);

                // get random password
                Rng.GetBytes(bytes);

                // make sure they are valid ASCII characters
                for (int i = 0; i < bytes.Length; i++)
                {
                    bool changed = false;
                    while (bytes[i] < 33)
                    {
                        bytes[i] += 31;
                        changed = true;
                    }

                    while (bytes[i] >= 127)
                    {
                        bytes[i] -= 39;
                        changed = true;
                    }

                    while (!char.IsLetterOrDigit((char) bytes[i]) && !AcceptablePunctuation.Contains((char) bytes[i]))
                    {
                        bytes[i] -= 19;
                        changed = true;
                    }

                    if (changed)
                    {
                        i--; // reprocess to make sure it still satisfies all rules
                    }
                }

                if(requireNumber)
                {
                    bool numberFound = false;
                    for (int i = 0; i < bytes.Length; i++)
                    {
                        if (char.IsDigit((char) bytes[i]))
                        {
                            numberFound = true;
                            break;
                        }
                    }

                    if (!numberFound)
                    {
                        Rng.GetBytes(tempBytes);
                        int changedIndex = tempBytes[0] % length;
                        changedIndexes[0] = changedIndex;
                        bytes[changedIndex] = (byte)(tempBytes[1] % 10 + '0');
                    }
                }

                if (requireSymbol)
                {
                    bool symbolFound = false;
                    for (int i = 0; i < bytes.Length; i++)
                    {
                        if (AcceptablePunctuation.Contains((char) bytes[i]))
                        {
                            symbolFound = true;
                            break;
                        }
                    }

                    if (!symbolFound)
                    {
                        int changedIndex = -1;
                        while (changedIndex == -1 || changedIndexes.Contains(changedIndex))
                        {
                            Rng.GetBytes(tempBytes);
                            changedIndex = tempBytes[0] % length;
                            
                        }
                        changedIndexes[1] = changedIndex;
                        bytes[changedIndex] = (byte)(AcceptablePunctuation[tempBytes[0] % AcceptablePunctuation.Length]);
                    }
                }

                if (requiresUppercaseAndLowercaseChar)
                {
                    bool upperFound = false;
                    bool lowerFound = false;
                    for (int i = 0; i < bytes.Length; i++)
                    {
                        if (char.IsUpper((char) bytes[i]))
                        {
                            upperFound = true;
                        }

                        if (char.IsLower((char) bytes[i]))
                        {
                            lowerFound = true;
                        }
                    }

                    if (!upperFound)
                    {
                        Rng.GetBytes(tempBytes);
                        int changedIndex = -1;
                        while (changedIndex == -1 || changedIndexes.Contains(changedIndex))
                        {
                            Rng.GetBytes(tempBytes);
                            changedIndex = tempBytes[0] % length;
                            
                        }
                        changedIndexes[2] = changedIndex;
                        bytes[changedIndex] = (byte)(tempBytes[1] % ('Z' - 'A') + 'A');
                    }

                    if (!lowerFound)
                    {
                        Rng.GetBytes(tempBytes);
                        int changedIndex = -1;
                        while (changedIndex == -1 || changedIndexes.Contains(changedIndex))
                        {
                            Rng.GetBytes(tempBytes);
                            changedIndex = tempBytes[0] % length;
                            
                        }
                        changedIndexes[3] = changedIndex;
                        bytes[changedIndex] = (byte)(tempBytes[1] % ('z' - 'a') + 'a');
                    }
                }

                // check there are no forbidden words (or close matches) - swear words etc.
                string result = Encoding.ASCII.GetString(bytes);
                if (ForbiddenWordMatcher.GetClosestMatchOrDefault(result, 0.51)) // this can be a little slow
                {
                    continue;
                }

                return result;
            }
        }
    }
}
