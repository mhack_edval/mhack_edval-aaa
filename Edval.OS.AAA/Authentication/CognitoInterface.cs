﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using Edval.OS.AAA.Extensions;
using Microsoft.Extensions.Logging;

namespace Edval.OS.AAA.Authentication
{
    public class CognitoInterface
    {
        private readonly AmazonCognitoIdentityProviderClient _client;
        private readonly string _clientId;
        private readonly string _poolId;
        private readonly ILogger<CognitoInterface> _logger;

        public CognitoInterface(AWSCredentials credentials, RegionEndpoint region, string clientId, string poolId, ILogger<CognitoInterface> logger)
        {
            if (clientId.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(_clientId));
            }

            if (poolId.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(_poolId));
            }

            _clientId = clientId;
            _poolId = poolId;
            _logger = logger;

            _client = new AmazonCognitoIdentityProviderClient(credentials, new AmazonCognitoIdentityProviderConfig()
            {
                RegionEndpoint = region,
            });
        }

        public async Task<(bool success, string? userName, string? userPassword, string? userId)> SignUp(string userEmail, string userName)
        {
            string password = PasswordGenerator.RandomPassword();

            List<AttributeType> attributes = new List<AttributeType>();
            attributes.Add(new AttributeType()
            {
                Name = "email",
                Value = userEmail
            });

            AdminCreateUserResponse? response = await _client.AdminCreateUserAsync(new AdminCreateUserRequest()
            {
                UserPoolId = _poolId,
                TemporaryPassword = password,
                Username = userName,
                UserAttributes = attributes,
                MessageAction = MessageActionType.SUPPRESS
            });

            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                _logger.LogError("Failed creating user. Code: " + response.HttpStatusCode);
                return (false, null, null, null);
            }

            _logger.LogInformation($"Successfully created user {userEmail}.");
            return (true, userName, password, response.User.Attributes.Single(x => x.Name == "sub").Value);
        }

        public class LoginResult
        {
            // logged in result
            public bool UserLoggedIn { get; private set; }
            public string? AccessToken { get; private set; }
            public string? FailAdvice { get; private set; }

            // challenges
            public bool UserNeedsNewPassword { get; private set; }
            public string? SessionId { get; private set; }

            private LoginResult()
            {

            }

            public static LoginResult Success(string accessToken)
            {
                return new LoginResult()
                {
                    UserLoggedIn = true,
                    AccessToken = accessToken
                };
            }

            public static LoginResult Failed(string? failAdvice)
            {
                return new LoginResult()
                {
                    UserLoggedIn = false,
                    AccessToken = null,
                    FailAdvice = failAdvice
                };
            }

            public static LoginResult NewPasswordChallenge(string sessionId)
            {
                return new LoginResult()
                {
                    UserLoggedIn = false,
                    AccessToken = null,
                    UserNeedsNewPassword = true,
                    SessionId = sessionId
                };
            }
        }

        public async Task<LoginResult> Login(string username, string password)
        {
            try
            {
                AdminInitiateAuthResponse? result = await _client.AdminInitiateAuthAsync(new AdminInitiateAuthRequest()
                {
                    UserPoolId = _poolId,
                    ClientId = _clientId,
                    AuthFlow = AuthFlowType.ADMIN_NO_SRP_AUTH,
                    AuthParameters = new Dictionary<string, string>()
                    {
                        {"USERNAME", username},
                        {"PASSWORD", password}
                    }
                });

                _logger.LogInformation("Successfully logged in " + username);
                return result.ChallengeName == "NEW_PASSWORD_REQUIRED"
                    ? LoginResult.NewPasswordChallenge(result.Session)
                    : LoginResult.Success(result.AuthenticationResult.AccessToken);
            }
            catch (NotAuthorizedException e)
            {
                _logger.LogInformation($"Failed to login {username}: " + e.Message);
                return LoginResult.Failed(e.Message);
            }
            catch (InvalidParameterException e)
            {
                _logger.LogInformation($"Failed to login {username}: " + e);
                return LoginResult.Failed(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Failed to login {username}: " + e);
                Console.WriteLine("Failed to login reason: " + e.ToString());
                return LoginResult.Failed(null); // TODO
            }
        }

        public async Task<LoginResult> RespondToPasswordChallenge(string newPassword, string username, string sessionId)
        {
            try
            {
                AdminRespondToAuthChallengeResponse? result = await _client.AdminRespondToAuthChallengeAsync(new AdminRespondToAuthChallengeRequest()
                {
                    ChallengeName = "NEW_PASSWORD_REQUIRED",
                    ClientId = _clientId,
                    UserPoolId = _poolId,
                    Session = sessionId,
                    ChallengeResponses = new Dictionary<string, string>()
                    {
                        { "NEW_PASSWORD", newPassword },
                        { "USERNAME", username }
                    }
                });

                return result.ChallengeName == "NEW_PASSWORD_REQUIRED" ?
                    LoginResult.NewPasswordChallenge(result.Session) :
                    LoginResult.Success(result.AuthenticationResult.AccessToken);
            }
            catch (Exception e)
            {
                return LoginResult.Failed(e.ToString());
            }
        }

        public async Task<bool> ChangeUserPassword(string newPassword, string username)
        {
            try
            {
                AdminSetUserPasswordRequest request = new AdminSetUserPasswordRequest()
                {
                    UserPoolId = _poolId,
                    Username = username,
                    Password = newPassword,
                    Permanent = true
                };

                _logger.LogInformation($"Changing {username}'s password");
                await _client.AdminSetUserPasswordAsync(request);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Changing {username}'s password failed");
                return false;
            }
        }

        public async Task DeleteUser(string userName)
        {
            _logger.LogInformation($"Deleting user {userName}.");
            await _client.AdminDeleteUserAsync(new AdminDeleteUserRequest()
            {
                UserPoolId = _poolId,
                Username = userName
            });
        }

        public class GetUserResponse
        {
            public string UserId { get; set; }
        }

        public async Task<GetUserResponse?> GetUser(string userName)
        {
            try
            {
                var userResponse = await _client.AdminGetUserAsync(new AdminGetUserRequest()
                {
                    UserPoolId = _poolId,
                    Username = userName
                });

                return new GetUserResponse() {UserId = userResponse.UserAttributes.First(x => x.Name == "sub").Value};
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed getting user");
                return null;
            }
        }       
    }
}
