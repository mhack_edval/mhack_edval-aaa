﻿using Amazon;
using Edval.OS.AAA.Extensions;

namespace Edval.OS.AAA.Authentication
{
    public class CognitoConfiguration
    {
        public RegionEndpoint? AwsRegion => Region.IsNullOrEmpty() ? null : RegionEndpoint.GetBySystemName(Region);
        public string? Region { get; set; }
        public string? ClientId { get; set; }
        public string? PoolId { get; set; }
        public string? ValidTokenIssuer { get; set; }
        public string? PublicKeyUrl { get; set; }
        public string? AccessKeyId { get; set; }
        public string? SecretAccessKey { get; set; }
    }
}