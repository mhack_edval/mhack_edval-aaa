﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Edval.OS.AAA.Extensions;

namespace Edval.OS.AAA.Authentication
{
    public class FuzzyStringMatcher<TValue>
    {
        private class MatchResult
        {
            public TValue MatchValue { get; }
            public string MatchKey { get; }
            public string InputKey { get; }
            public double MatchPercent { get; }

            public MatchResult(TValue matchValue, string matchKey, string inputKey, double matchPercent)
            {
                MatchValue = matchValue;
                MatchKey = matchKey;
                InputKey = inputKey;
                MatchPercent = matchPercent;
            }
        }

        private readonly ConcurrentDictionary<string, MatchResult> _matches;
        private readonly ConcurrentDictionary<string, TValue> _values;
        private readonly bool _caseInsensitive;
        [MaybeNull] private readonly TValue _defaultValue;
        private readonly bool _cacheMatches;

        public FuzzyStringMatcher([NotNull] Func<TValue, string> keySelector, [NotNull] List<TValue> input, bool caseInsensitive = false, [MaybeNull] TValue defaultValue = default, bool cacheMatches = true)
        {
            _values = new ConcurrentDictionary<string, TValue>();
            _matches = new ConcurrentDictionary<string, MatchResult>();
            _caseInsensitive = caseInsensitive;
            _defaultValue = defaultValue;
            _cacheMatches = cacheMatches;

            for (int i = 0; i < input.Count; i++)
            {
                TValue value = input[i];
                string key = ToLowerIfCaseInsensitive(keySelector(value));

                bool result = _matches.TryAdd(key, new MatchResult(value, key, key, 1));
                if (!result)
                {
                    throw new Exception("Failed adding item to dictionary. Is there a duplicate?");
                }

                result = _values.TryAdd(key, value);
                if (!result)
                {
                    throw new Exception("Failed adding item to dictionary. Is there a duplicate?");
                }
            }
        }

        public FuzzyStringMatcher(Dictionary<string, TValue> baseDictionary, bool caseInsensitive = false, TValue defaultValue = default, bool cacheMatches = true)
        {
            _values = new ConcurrentDictionary<string, TValue>();
            _matches = new ConcurrentDictionary<string, MatchResult>();
            _caseInsensitive = caseInsensitive;
            _defaultValue = defaultValue;
            _cacheMatches = cacheMatches;

            foreach (KeyValuePair<string, TValue> pair in baseDictionary)
            {
                string key = ToLowerIfCaseInsensitive(pair.Key);
                bool result = _matches.TryAdd(key, new MatchResult(pair.Value, key, key, 1));
                if (!result)
                {
                    throw new Exception("Failed adding item to dictionary. Is there a duplicate?");
                }

                result = _values.TryAdd(key, pair.Value);
                if (!result)
                {
                    throw new Exception("Failed adding item to dictionary. Is there a duplicate?");
                }
            }
        }

        private string ToLowerIfCaseInsensitive(string val)
        {
            if (_caseInsensitive && HasAnyUpper(val))
            {
                return val.ToLower();
            }

            return val;
        }

        private static bool HasAnyUpper(string s)
        {
            if (s.IsNullOrEmpty())
            {
                return false;
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsUpper(s[i]))
                {
                    return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// Get the closest match.
        /// </summary>
        /// <param name="minimumPercent">The minimum percent for a match. [0..1] range.</param>
        [return: MaybeNull]
        public TValue GetClosestMatchOrDefault(string? key, double minimumPercent = 0.75)
        {
            if (key == null)
            {
                return _defaultValue;
            }

            key = ToLowerIfCaseInsensitive(key);
            if (_matches.TryGetValue(key, out MatchResult? match))
            {
                Debug.Assert(key == match.InputKey);
                if (match.MatchPercent >= minimumPercent)
                {
                    return match.MatchValue;
                }

                return _defaultValue;
            }

            KeyValuePair<string, TValue> closestMatch = default;
            double? highestPercent = null;
            foreach (KeyValuePair<string, TValue> valuePair in _values)
            {
                double matchPercent = Match(key, valuePair.Key);
                if (highestPercent == null || matchPercent > highestPercent)
                {
                    highestPercent = matchPercent;
                    closestMatch = valuePair;
                }
            }

            if (highestPercent == null)
            {
                return _defaultValue;
            }

            if (_cacheMatches)
            {
                _matches.TryAdd(key, new MatchResult(closestMatch.Value, closestMatch.Key, key, highestPercent.Value));
            }

            return highestPercent >= minimumPercent ? closestMatch.Value : _defaultValue;
        }

        private static double Match(string? val1, string? val2)
        {
            int longestLength = val1?.Length > val2?.Length ? val1?.Length ?? 0 : val2?.Length ?? 0;
            if (longestLength == 0)
            {
                return 1; // must be both empty or null.
            }

            return 1.0 - (LevenshteinDistance(val1, val2) / (double) longestLength);
        }

        /// <summary>
        /// From https://github.com/DanHarltey/Fastenshtein/blob/master/src/Fastenshtein/StaticLevenshtein.cs
        /// MIT Licence.
        ///
        /// Edited to use stackalloc for less GC. However this means it can't handle super long strings.
        /// </summary>
        public static int LevenshteinDistance(string? value1, string? value2)
        {
            if (string.IsNullOrEmpty(value2))
            {
                return value1?.Length ?? 1;
            }

            if (string.IsNullOrEmpty(value1))
            {
                return value2.Length;
            }

            Span<int> costs = stackalloc int[value2.Length];

            // Add indexing for insertion to first row
            for (int i = 0; i < costs.Length;)
            {
                costs[i] = ++i;
            }

            for (int i = 0; i < value1.Length; i++)
            {
                // cost of the first index
                int cost = i;
                int addationCost = i;

                // cache value for inner loop to avoid index lookup and bonds checking, profiled this is quicker
                char value1Char = value1[i];

                for (int j = 0; j < value2.Length; j++)
                {
                    int insertionCost = cost;

                    cost = addationCost;

                    // assigning this here reduces the array reads we do, improvement of the old version
                    addationCost = costs[j];

                    if (value1Char != value2[j])
                    {
                        if (insertionCost < cost)
                        {
                            cost = insertionCost;
                        }

                        if (addationCost < cost)
                        {
                            cost = addationCost;
                        }

                        ++cost;
                    }

                    costs[j] = cost;
                }
            }

            return costs[^1];
        }
    }
}
