﻿using System;
using System.Collections.Generic;
using System.Text;
using Edval.OS.AAA.Models;

namespace Edval.OS.AAA.Extensions
{
    public static class PathPartExtensions
    {
        public static bool TryGetChild(this IList<PathPart> pathParts, string path, out PathPart? pathPart)
        {
            if (path.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(path), $"{nameof(path)} cannot be null or empty.");
            }

            for (int i = 0; i < pathParts.Count; i++)
            {
                if (pathParts[i].Value == path)
                {
                    pathPart = pathParts[i];
                    return true;
                }
            }

            pathPart = null;
            return false;
        }

        public static string ComposePath(this IReadOnlyList<PathPart> pathParts, char separator = '/')
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < pathParts.Count; i++)
            {
                sb.Append(pathParts[i].Value);
                sb.Append(separator);
            }

            if (sb.Length > 0)
            {
                sb.Length -= 1; // remove last separator
            }

            return sb.ToString();
        }
    }
}