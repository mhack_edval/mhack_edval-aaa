﻿using System;
using System.Buffers;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Text;

namespace Edval.OS.AAA.Extensions
{
    public static class StringExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsNullOrEmpty([NotNullWhen(false)] this string? s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static int? ParseIntOrDefault(this string? s)
        {
            if (s.IsNullOrEmpty())
            {
                return null;
            }

            if (int.TryParse(s, out int i))
            {
                return i;
            }

            return null;
        }

        public static double? ParseDoubleOrDefault(this string? s)
        {
            if (s.IsNullOrEmpty())
            {
                return null;
            }

            if (double.TryParse(s, out double d))
            {
                return d;
            }

            return null;
        }

        public static bool IsIn24HrFormat(this string? s) =>
            !s.IsNullOrEmpty() &&
            s.Length == 5 &&
            s[0].IsAsciiNumber() &&
            s[1].IsAsciiNumber() &&
            s[2] == ':' &&
            s[3].IsAsciiNumber() &&
            s[4].IsAsciiNumber();

        private static bool IsAsciiNumber(this char c) => c >= '0' && c <= '9';

        public static string? FromBase64UrlToString(this ReadOnlySpan<char> base64Url)
        {
            return base64Url.FromBase64Url<string>();
        }

        public static byte[]? FromBase64UrlToByteArray(this ReadOnlySpan<char> base64Url)
        {
            return base64Url.FromBase64Url<byte[]>();
        }

        public static string? FromBase64UrlToString(this string base64Url)
        {
            return base64Url.AsSpan().FromBase64Url<string>();
        }

        public static byte[]? FromBase64UrlToByteArray(this string base64Url)
        {
            return base64Url.AsSpan().FromBase64Url<byte[]>();
        }

        /// <summary>
        /// Warning: only accepts string or byte[] as T.
        /// </summary>
        /// <typeparam name="T">Warning: only accepts string or byte[] as T.</typeparam>
        /// <param name="base64Url"></param>
        /// <returns></returns>
        private static T? FromBase64Url<T>(this ReadOnlySpan<char> base64Url) where T : class
        {
            if (base64Url.IsEmpty)
            {
                return default;
            }

            char[]? base64CharArray = null;
            byte[]? byteArray = null;

            try
            {
                // apparently you can't have more than 2 padding chars...
                int length = base64Url.Length + (base64Url.Length % 4);
                if (length - base64Url.Length == 3)
                {
                    length -= 2;
                }

                // copy
                base64CharArray = ArrayPool<char>.Shared.Rent(length);
                Span<char> base64 = ((Span<char>)base64CharArray).Slice(0, length);

                base64Url.CopyTo(base64);

                // add padding
                for (int i = base64Url.Length; i < length; i++)
                {
                    base64[i] = '=';
                }

                // convert '_' to '/' and '-' to '+'
                for (int i = 0; i < length; i++)
                {
                    switch (base64[i])
                    {
                        case '_':
                            base64[i] = '/';
                            break;
                        case '-':
                            base64[i] = '+';
                            break;
                    }
                }

                int byteSpanLength = (int)Math.Floor((base64Url.Length / 4.0) * 3);
                byteArray = ArrayPool<byte>.Shared.Rent(byteSpanLength);
                Span<byte> byteSpan = ((Span<byte>)byteArray).Slice(0, byteSpanLength);
                if (!Convert.TryFromBase64Chars(base64, byteSpan, out int bytesWritten))
                {
                    // should tell us the error because the above message does not but I wanted to avoid the unnecessary .ToString() because of GC.
                    Convert.FromBase64String(base64.ToString());
                    throw new Exception("Failed converting value to bytes.");
                }

                if (typeof(T) == typeof(string))
                {
                    return Encoding.ASCII.GetString(byteSpan) as T;
                }
                else if (typeof(T) == typeof(byte[]))
                {
                    return byteSpan.Slice(0, bytesWritten).ToArray() as T;
                }
                else
                {
                    throw new NotSupportedException("Only strings and byte[] are supported.");
                }
            }
            finally
            {
                if (base64CharArray != null)
                {
                    ArrayPool<char>.Shared.Return(base64CharArray);
                }

                if (byteArray != null)
                {
                    ArrayPool<byte>.Shared.Return(byteArray);
                }
            }
        }
    }
}
