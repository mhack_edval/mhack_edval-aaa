﻿using System;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Extensions
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Attempts to parse a permission from a string.
        /// Will return Permission.None if it fails.
        /// </summary>
        public static Permission ParsePermission(this string s)
        {
            if (s.IsNullOrEmpty())
            {
                return Permission.None;
            }

            if (Enum.TryParse(s, true, out Permission permission))
            {
                return permission;
            }

            return s == "*" ? Permission.All : Permission.None;
        }

        /// <summary>
        /// Attempts to parse a location from a string.
        /// Will return PathPolicyVariableLocation.None if it fails.
        /// </summary>
        public static PathPolicyVariableLocation ParseLocation(this string s)
        {
            if (s.IsNullOrEmpty())
            {
                return PathPolicyVariableLocation.None;
            }

            if (Enum.TryParse(s, true, out PathPolicyVariableLocation location))
            {
                return location;
            }

            return PathPolicyVariableLocation.None;
        }

        /// <summary>
        /// Attempts to parse a HttpMethod from a string.
        /// Will return HttpMethod.None if it fails.
        /// </summary>
        public static HttpMethod ParseHttpMethod(this string s)
        {
            if (s.IsNullOrEmpty())
            {
                return HttpMethod.None;
            }

            if (Enum.TryParse(s, true, out HttpMethod httpMethod))
            {
                return httpMethod;
            }

            return HttpMethod.None;
        }

        public static bool IsDefined<T>(this T e) where T : Enum
        {
            return Enum.IsDefined(typeof(T), e);
        }
    }
}
