﻿using System;
using System.Collections.Generic;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Extensions
{
    public static class UserPolicyExtensions
    {
        public static bool TryGetChild(this List<UserPolicyPath> pathParts, string path, out UserPolicyPath? pathPart)
        {
            if (path.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(path), $"{nameof(path)} cannot be null or empty.");
            }

            for (int i = 0; i < pathParts.Count; i++)
            {
                if (pathParts[i].Path == path)
                {
                    pathPart = pathParts[i];
                    return true;
                }
            }

            pathPart = null;
            return false;
        }

        public static UserPolicyPath GetChild(this List<UserPolicyPath> pathParts, string path)
        {
            if (path.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(path), $"{nameof(path)} cannot be null or empty.");
            }

            for (int i = 0; i < pathParts.Count; i++)
            {
                if (pathParts[i].Path == path)
                {
                    return pathParts[i];
                }
            }

            throw new Exception("Couldn't find path.");
        }
    }
}