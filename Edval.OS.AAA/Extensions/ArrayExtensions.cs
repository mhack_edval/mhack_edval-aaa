﻿using System.Collections.Generic;

namespace Edval.OS.AAA.Extensions
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Array contains method. Quicker than linq (and doesn't box variables).
        /// </summary>
        public static bool Contains<T>(this T[] array, T el) where T : struct
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (EqualityComparer<T>.Default.Equals(array[i], el))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
