using System;
using System.Text.Json;
using Amazon.Runtime;
using Edval.OS.AAA.ApiKeys;
using Edval.OS.AAA.Authentication;
using Edval.OS.AAA.Configuration;
using Edval.OS.AAA.Database;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.JWT.PublicKeys;
using Edval.OS.AAA.Middleware;
using Edval.OS.AAA.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AuthDbContext = Edval.OS.AAA.Database.PostGres.AuthDbContext;

namespace Edval.OS.AAA
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var cookieConfig = Configuration.GetSection("AuthCookie").Get<AuthCookieConfiguration>();
            if (cookieConfig?.Domain.IsNullOrEmpty() != false)
            {
                throw new Exception("Missing cookie configuration");
            }

            Console.WriteLine("Cookie config: " + JsonSerializer.Serialize(cookieConfig));

            services.AddSingleton(cookieConfig);

            services.AddAuthentication().AddApiKeySupport();
            services.AddAuthorization(options =>
            {
                options.AddPolicy("api-key",
                    builder => builder
                        .RequireAuthenticatedUser()
                        .AddAuthenticationSchemes(ApiKeyAuthenticationOptions.DefaultScheme));
            });

            services.AddControllers();

            string authDbContextConnectionString = Configuration.GetConnectionString("AuthDb");
            services.AddDbContext<AuthDbContext>(options =>
            {
                options.UseNpgsql(authDbContextConnectionString);
            });
            services.AddScoped<AuthRepo>();
            services.AddScoped<IApiKeyRepo, AuthRepo>();

            CognitoConfiguration config = Configuration.GetSection("Cognito").Get<CognitoConfiguration>();
            services.AddSingleton(config);

            Console.WriteLine("Dummy instruction 1");
            Console.WriteLine("Dummy instruction 2");
            Console.WriteLine("Dummy instruction 3");
            Console.WriteLine("Dummy instruction 4");
            Console.WriteLine("Dummy instruction 5");
            Console.WriteLine("Dummy instruction 6");
            Console.WriteLine("Dummy instruction 7");

            CognitoConfiguration config2 = null;
            config2.ClientId = "xxxxxxx";

            services.AddTransient<PolicyEnforcer>();
            services.AddTransient(serviceProvider =>
                new RequestAuthoriser<CognitoPublicKeys>(config.ValidTokenIssuer, config.PublicKeyUrl,
                    serviceProvider.GetRequiredService<AuthRepo>(), 
                    serviceProvider.GetRequiredService<PolicyEnforcer>())
            );

            var credentials = new BasicAWSCredentials(config.AccessKeyId, config.SecretAccessKey);
            services.AddScoped(services => new CognitoInterface(credentials, 
                config.AwsRegion, config.ClientId, config.PoolId, 
                services.GetRequiredService<ILogger<CognitoInterface>>()));
            PublicKeyCache<CognitoPublicKeys>.GetKeys(config.PublicKeyUrl).Wait();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AuthDbContext authDbContext, IApiKeyRepo apiKeyRepo, AuthRepo authRepo, CognitoInterface cognito)
        {
            authDbContext.Database.Migrate();

            app.UseNoCacheByDefault();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
