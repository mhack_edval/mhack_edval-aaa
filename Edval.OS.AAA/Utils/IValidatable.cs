﻿namespace Edval.OS.AAA.Utils
{
    /// <summary>
    /// Used to keep validation similar.
    /// </summary>
    internal interface IValidatable
    {
        (bool valid, string? reason) Validate();
    }
}
