﻿using System.Text.Json;

namespace Edval.OS.AAA.Utils
{
    /// <summary>
    /// Serialisation helper methods.
    /// Applies default options.
    /// </summary>
    public static class JSON
    {
        private static readonly JsonSerializerOptions DefaultOptions = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true
        };

        /// <summary>
        /// Deserialize with our default options. 
        /// </summary>
        public static T Deserialize<T>(this string s)
        {
            return JsonSerializer.Deserialize<T>(s, DefaultOptions);
        }
    }
}
