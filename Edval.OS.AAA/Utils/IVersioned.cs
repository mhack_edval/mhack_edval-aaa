﻿namespace Edval.OS.AAA.Utils
{
    internal interface IVersioned
    {
        string Version { get; }
    }
}
