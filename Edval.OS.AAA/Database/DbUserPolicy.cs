﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Database
{
    public class DbUserPolicy
    {
        [Key]
        public string UserId { get; set; }
        public string Policy { get; set; }
        public ulong Version { get; set; }
        public DateTime DateUpdated { get; set; }

        private DbUserPolicy() { }
        
        public DbUserPolicy(UserPolicy userPolicy)
        {
            if (!System.Version.TryParse(userPolicy.Version, out Version? version))
            {
                throw new Exception("Failed parsing version");
            }

            UserId = userPolicy.UserId;
            Policy = JsonSerializer.Serialize(userPolicy);
            Version = VersionUtils.VersionToULong(version);
            DateUpdated = DateTime.Now;
        }
    }
}
