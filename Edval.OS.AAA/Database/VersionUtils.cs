﻿using System;

namespace Edval.OS.AAA.Database
{
    public static class VersionUtils
    {
        private static ulong MajorMask    = 0b1111111111111111000000000000000000000000000000000000000000000000UL;
        private static ulong MinorMask    = 0b0000000000000000111111111111111100000000000000000000000000000000UL;
        private static ulong BuildMask    = 0b0000000000000000000000000000000011111111111111110000000000000000UL;
        private static ulong RevisionMask = 0b0000000000000000000000000000000000000000000000001111111111111111UL;

        public static Version ULongToVersion(ulong version)
        {
            int major = (int)((version & MajorMask) >> 48);
            int minor = (int)((version & MinorMask) >> 32);
            int build = (int)((version & BuildMask) >> 16);
            int revision = (int)(version & RevisionMask);
            Version result = new Version(major, minor, build, revision);
            Check(result);
            return result;
        }

        public static ulong VersionToULong(Version version)
        {
            // this is because version.Build and version.Revision are -1 if they aren't specified.
            version = new Version(version.Major,
                version.Minor, 
                Math.Max(0, version.Build),
                Math.Max(0, version.Revision));
            Check(version);
            ulong major = ((ulong)version.Major) << 48;
            ulong minor = ((ulong)version.Minor) << 32;
            ulong build = ((ulong)version.Build) << 16;
            ulong revision = (ulong)version.Revision;
            return major | minor | build | revision;
        }

        private static void Check(Version version)
        {
            if (version.Major < 0 || version.Major > ushort.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(version) + "." + nameof(version.Major), version.Major,
                    "Version components must be within ushort range.");
            }

            if (version.Minor < 0 || version.Minor > ushort.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(version) + "." + nameof(version.Minor), version.Minor,
                    "Version components must be within ushort range.");
            }

            if (version.Build < 0 || version.Build > ushort.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(version) + "." + nameof(version.Build), version.Build,
                    "Version components must be within ushort range.");
            }

            if (version.Revision < 0 || version.Revision > ushort.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(version) + "." + nameof(version.Revision), version.Revision,
                    "Version components must be within ushort range.");
            }
        }
    }
}
