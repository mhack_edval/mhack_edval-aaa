﻿using System.Threading.Tasks;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Database
{
    public interface IAuthRepo
    {
        /// <summary>
        /// Retrieves a user policy from the database.
        /// </summary>
        Task<UserPolicy?> GetUserPolicy(string userId);

        /// <summary>
        /// Puts a user policy into the database.
        /// </summary>
        Task SetUserPolicy(UserPolicy policy);

        /// <summary>
        /// Retrieves a path policy from the database.
        /// </summary>
        Task<PathPolicy?> GetPathPolicy(string path);

        /// <summary>
        /// Puts a path policy into the database.
        /// </summary>
        Task SetPathPolicy(PathPolicy policy);
    }
}
