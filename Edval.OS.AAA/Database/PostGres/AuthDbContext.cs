﻿using Edval.OS.AAA.ApiKeys;
using Microsoft.EntityFrameworkCore;

namespace Edval.OS.AAA.Database.PostGres
{
    /// <summary>
    /// Use AuthRepo instead!
    /// </summary>
    public class AuthDbContext : DbContext
    {
        public DbSet<ApiKey> ApiKeys { get; set; }
        public DbSet<DbPathPolicy> PathPolicies { get; set; }
        public DbSet<DbUserPolicy> UserPolicies { get; set; }

        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbUserPolicy>().HasIndex(x => x.UserId);
            modelBuilder.Entity<DbPathPolicy>().HasIndex(x => x.Key);

            // pk
            modelBuilder.Entity<ApiKey>().HasKey(x => x.Key);
        }
    }
}
