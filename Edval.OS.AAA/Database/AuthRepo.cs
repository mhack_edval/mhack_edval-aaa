﻿using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Edval.OS.AAA.ApiKeys;
using Edval.OS.AAA.Database.PostGres;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Edval.OS.AAA.Database
{
    public class AuthRepo : IAuthRepo, IApiKeyRepo
    {
        private readonly AuthDbContext _dbContext;
        private readonly ILogger<AuthRepo> _logger;

        public AuthRepo(AuthDbContext dbContext, ILogger<AuthRepo> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<UserPolicy?> GetUserPolicy(string userId)
        {
            var dbPolicy = await _dbContext.UserPolicies.FirstOrDefaultAsync(x => x.UserId == userId);
            if (dbPolicy != null && !dbPolicy.Policy.IsNullOrEmpty())
            {
                return JsonSerializer.Deserialize<UserPolicy>(dbPolicy.Policy);
            }

            return null;
        }

        public async Task SetUserPolicy(UserPolicy policy)
        {
            using var scope = _logger.BeginScope("Setting user policy {PolicyId}", policy.UserId);

            var newDbPolicy = new DbUserPolicy(policy);
            await _dbContext.UserPolicies.Upsert(newDbPolicy)
                .UpdateIf((existingPol, newPol) => newPol.Version >= existingPol.Version)
                .RunAsync();
        }

        public async Task DeleteUserPolicy(string userId)
        {
            _dbContext.UserPolicies.RemoveRange(_dbContext.UserPolicies.Where(x => x.UserId == userId));
            await _dbContext.SaveChangesAsync();
        }

        public async Task<PathPolicy?> GetPathPolicy(string path)
        {
            var dbPolicy = await _dbContext.PathPolicies.FirstOrDefaultAsync(x => x.Key == path);
            if (dbPolicy != null && !dbPolicy.Policy.IsNullOrEmpty())
            {
                return JsonSerializer.Deserialize<PathPolicy>(dbPolicy.Policy);
            }

            return null;
        }

        public async Task SetPathPolicy(PathPolicy policy)
        {
            policy.PrepareForDb();
            using var scope = _logger.BeginScope("Setting path policy {PolicyId}", policy.Key);

            var newDbPolicy = new DbPathPolicy(policy);
            await _dbContext.PathPolicies.Upsert(newDbPolicy)
                .UpdateIf((existingPol, newPol) => newPol.Version >= existingPol.Version)
                .RunAsync();
        }

        public async Task DeletePathPolicy(string path)
        {
            _dbContext.PathPolicies.RemoveRange(_dbContext.PathPolicies.Where(x => x.Key == path));
            await _dbContext.SaveChangesAsync();
        }

        public Task<ApiKey?> GetApiKey(string apiKey)
        {
            return _dbContext.ApiKeys.SingleOrDefaultAsync(x => x.Key == apiKey)!;
        }

        public async Task PutApiKey(ApiKey apiKey)
        {
            ApiKey? existingKey = await GetApiKey(apiKey.Key);
            if (existingKey != null)
            {
                _dbContext.ApiKeys.Remove(existingKey);
            }

            _dbContext.ApiKeys.Add(apiKey);
            await _dbContext.SaveChangesAsync();
        }
    }
}
