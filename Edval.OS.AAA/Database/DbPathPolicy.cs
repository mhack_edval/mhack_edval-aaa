﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Database
{
    public class DbPathPolicy
    {
        [Key]
        public string Key { get; set; }
        public string Policy { get; set; }
        public ulong Version { get; set; }
        public DateTime DateUpdated { get; set; }

        private DbPathPolicy() { }

        public DbPathPolicy(PathPolicy pathPolicy)
        {
            pathPolicy.PrepareForDb();
            if (!System.Version.TryParse(pathPolicy.Version, out Version? version))
            {
                throw new Exception("Failed parsing version");
            }

            Key = pathPolicy.Key;
            Policy = JsonSerializer.Serialize(pathPolicy);
            Version = VersionUtils.VersionToULong(version);
            DateUpdated = DateTime.Now;
        }
    }
}
