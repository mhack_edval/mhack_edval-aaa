﻿using System.Threading;
using System.Threading.Tasks;

namespace Edval.OS.AAA.JWT.PublicKeys
{
    /// <summary>
    /// A cache for a public key. Thread safe.
    /// </summary>
    /// <typeparam name="TKeyType">The type of public key to store and cache.</typeparam>
    public class PublicKeyCache<TKeyType> where TKeyType : class, IPublicKeys, new()
    {
        // A static field in a generic type will not be
        // shared among instances of different close
        // constructed types.

        // This means that for a generic class C<T> which
        // has a static field X, the values of C<int>.X
        // and C<string>.X have completely different,
        // independent values.
        // ReSharper disable once StaticMemberInGenericType
        private static readonly SemaphoreSlim Lock = new SemaphoreSlim(1);
        private static TKeyType? _cache;

        private PublicKeyCache() { }

        public static async Task<TKeyType> GetKeys(string url)
        {
            await Lock.WaitAsync();
            try
            {
                if (_cache != null)
                {
                    return _cache;
                }

                TKeyType keys = new TKeyType();
                await keys.InitAsync(url);
                _cache = keys;
                return keys;
            }
            finally
            {
                Lock.Release();
            }
        }

        public static async Task ClearCache()
        {
            await Lock.WaitAsync();
            try
            {
                _cache = null;
            }
            finally
            {
                Lock.Release();
            }
        }
    }
}
