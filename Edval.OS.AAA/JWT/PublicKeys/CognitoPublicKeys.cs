﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.JWT.PublicKeys
{
    public class CognitoPublicKeys : IPublicKeys
    {
        public IReadOnlyDictionary<string, PublicKey>? Keys { get; set; }

        public async Task InitAsync(string url)
        {
            try
            {
                using HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url);
                string textResponse = await response.Content.ReadAsStringAsync();
                Dictionary<string, List<PublicKey>> keysJson = textResponse.Deserialize<Dictionary<string, List<PublicKey>>>();
                Keys = new ReadOnlyDictionary<string, PublicKey>(keysJson["keys"].ToDictionary(x => x.KId));
            }
            catch (Exception e)
            {
                Keys = null;
                await Console.Error.WriteAsync(e.ToString());
            }
        }
    }
}
