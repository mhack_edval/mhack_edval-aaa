﻿using System;
using Edval.OS.AAA.Extensions;
using JsonWebToken;

namespace Edval.OS.AAA.JWT.PublicKeys
{
    public class PublicKey
    {
        /// <summary>
        /// The algorithm of the public key.
        /// </summary>
        public string? Alg
        {
            get => _alg;
            set
            {
                _alg = value;
                _dirty = true;
            }
        }

        /// <summary>
        /// The ID of the public key.
        /// </summary>
        public string? KId
        {
            get => _kId;
            set
            {
                _kId = value;
                _dirty = true;
            }
        }

        /// <summary>
        /// The type of the public key.
        /// </summary>
        public string? KTy
        {
            get => _kTy;
            set
            {
                _kTy = value;
                _dirty = true;
            }
        }

        /// <summary>
        /// What the public key is used for.
        /// </summary>
        public string? Use
        {
            get => _use;
            set
            {
                _use = value;
                _dirty = true;
            }
        }

        public string? N
        {
            get => _n == null ? null : Convert.ToBase64String(_n);
            set
            {
                _n = value?.FromBase64UrlToByteArray();
                _dirty = true;
            }
        }

        public byte[]? NBytes
        {
            get => _n;
            set
            {
                _n = value;
                _dirty = true;
            }
        }

        public string? E
        {
            get => _e == null ? null : Convert.ToBase64String(_e);
            set
            {
                _e = value?.FromBase64UrlToByteArray();
                _dirty = true;
            }
        }

        public byte[] EBytes
        {
            get => _e;
            set
            {
                _e = value;
                _dirty = true;
            }
        }

        private byte[]? _n;
        private byte[]? _e;
        private string? _use;
        private string? _kTy;
        private string? _kId;
        private string? _alg;
        private bool _dirty;
        private AsymmetricJwk? _keyCache;

        /// <summary>
        /// Get the public key as an AsymmetricJwk.
        /// Will be cached so is not expensive to call unless the key continually changes.
        /// </summary>
        public Jwk? GetJwk()
        {
            if (_keyCache != null && !_dirty)
            {
                return _keyCache;
            }

            if (Alg?.ToLower() != "rs256")
            {
                throw new InvalidOperationException("Key is not RSA.");
            }

            if (Use?.ToLower() != "sig")
            {
                throw new InvalidOperationException($"Unknown key use {Use}.");
            }

            if (NBytes == null || NBytes.Length == 0)
            {
                throw new InvalidOperationException($"{nameof(NBytes)} is null or empty.");
            }

            if (EBytes == null || EBytes.Length == 0)
            {
                throw new InvalidOperationException($"{nameof(EBytes)} is null or empty.");
            }

            _keyCache = new RsaJwk(EBytes, NBytes, SignatureAlgorithm.RsaSha256);
            _dirty = false;
            return _keyCache;
        }
    }
}