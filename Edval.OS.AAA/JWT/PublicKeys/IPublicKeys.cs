﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Edval.OS.AAA.JWT.PublicKeys
{
    public interface IPublicKeys
    {
        IReadOnlyDictionary<string, PublicKey>? Keys { get; set; }

        Task InitAsync(string url);
    }
}