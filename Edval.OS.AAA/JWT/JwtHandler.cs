﻿using System;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.JWT.PublicKeys;
using JsonWebToken;

namespace Edval.OS.AAA.JWT
{
    public class JwtHandler
    {
        /// <summary>
        /// We only accept the token if this issuer is present.
        /// </summary>
        private readonly string _validIssuer;

        /// <summary>
        /// Where can we get public keys from to verify the signature?
        /// </summary>
        private readonly IPublicKeys? _publicKeys;

        private readonly JwtReader _reader;

        public enum Response
        {
            /// <summary>
            /// Default. This probably should have been set.
            /// </summary>
            None,

            /// <summary>
            /// Everything is good to go - token has been validated.
            /// </summary>
            Success,

            /// <summary>
            /// Can't find the public key from the public key source passed in the ctor.
            /// </summary>
            InvalidOrUnknownPublicKey,

            /// <summary>
            /// The token's signature is invalid or it is expired... etc.
            /// More info will be in the output - why it failed.
            /// </summary>
            InvalidToken
        }

        public JwtHandler(string validIssuer, IPublicKeys? publicKeys)
        {
            _validIssuer = validIssuer;
            _publicKeys = publicKeys;
            _reader = new JwtReader();
        }

        /// <summary>
        /// Attempts to validate and return a JWT.
        /// </summary>
        public Response TryValidateToken(string tokenText, SignatureAlgorithm? validSignature, out Jwt? token)
        {
            if (tokenText.IsNullOrEmpty())
            {
                throw new ArgumentException("Token text was null of empty.", nameof(tokenText));
            }

            if (validSignature == SignatureAlgorithm.None || validSignature == null)
            {
                throw new ArgumentException("Allowing a SignatureAlgorithm.None option is a security vulnerability so we do not allow it.");
            }

            int firstDotIndex = tokenText.IndexOf('.');
            if (firstDotIndex == -1)
            {
                token = null;
                return Response.InvalidToken;
            }

            // decode the header so we can find the right public key from the IPublicKeys
            string? decodedHeader = tokenText.AsSpan()
                .Slice(0, firstDotIndex)
                .FromBase64UrlToString();

            if (decodedHeader.IsNullOrEmpty())
            {
                token = null;
                return Response.InvalidToken;
            }

            const string startMarker = "\"kid\":\"";
            const string endMarker = "\"";
            int startIndex = decodedHeader.IndexOf(startMarker, StringComparison.InvariantCulture);
            int endIndex = decodedHeader.IndexOf(endMarker, startIndex + startMarker.Length, StringComparison.InvariantCulture);

            if (startIndex == -1 || endIndex == -1)
            {
                token = null;
                return Response.InvalidToken;
            }
            startIndex += startMarker.Length;
            ReadOnlySpan<char> id = decodedHeader.AsSpan().Slice(startIndex, endIndex - startIndex);
            PublicKey issuerKey = null;
            if (_publicKeys?.Keys?.TryGetValue(new string(id).Replace("\\/", "/"), out issuerKey) != true)
            {
                token = null;
                return Response.InvalidOrUnknownPublicKey;
            }

            // get the JWK
            var jwk = issuerKey?.GetJwk();
            if (jwk == null)
            {
                Console.WriteLine("Failed getting JWK");
                token = null;
                return Response.InvalidOrUnknownPublicKey;
            }

            // build the validation policy
            TokenValidationPolicy policy = new TokenValidationPolicyBuilder()
                .RequireSignature(jwk, validSignature)
                .RequireIssuer(_validIssuer)
                .EnableLifetimeValidation()
                .Build();

            // validate
            TokenValidationResult result = _reader.TryReadToken(tokenText, policy);

            // return
            if (result.Succedeed)
            {
                token = result.Token;
                return Response.Success;
            }
            else
            {
                Console.WriteLine("Failed validating token. Reason:\n" + result.Status);
                token = null;
                return Response.InvalidToken;
            }
        }
    }
}
