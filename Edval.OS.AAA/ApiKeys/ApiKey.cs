﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Edval.OS.AAA.ApiKeys
{
    public class ApiKey
    {
        public string Owner { get; set; }
        [Key] public string Key { get; set; }
        public DateTime Created { get; set; }
        public List<string> Roles { get; set; }

        public ApiKey()
        {
            
        }
        
        public ApiKey(string owner, string key, DateTime created, List<string> roles)
        {
            Owner = owner;
            Key = key;
            Created = created;
            Roles = roles;
        }
    }
}
