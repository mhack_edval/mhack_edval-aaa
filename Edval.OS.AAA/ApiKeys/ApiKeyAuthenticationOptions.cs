﻿using Microsoft.AspNetCore.Authentication;

namespace Edval.OS.AAA.ApiKeys
{
    public class ApiKeyAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "api-key";
        public string Scheme => DefaultScheme;
        public string AuthenticationType = DefaultScheme;
    }
}
