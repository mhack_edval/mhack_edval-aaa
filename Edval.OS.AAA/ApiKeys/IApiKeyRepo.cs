﻿using System.Threading.Tasks;

namespace Edval.OS.AAA.ApiKeys
{
    public interface IApiKeyRepo
    {
        Task<ApiKey?> GetApiKey(string apiKey);
        Task PutApiKey(ApiKey apiKey);
    }
}
