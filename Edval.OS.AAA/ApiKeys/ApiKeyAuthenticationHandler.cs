﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Edval.OS.AAA.ApiKeys
{
    public class ApiKeyAuthenticationHandler : AuthenticationHandler<ApiKeyAuthenticationOptions>
    {
        private const string ProblemDetailsContentType = "application/json";
        private readonly IApiKeyRepo _apiKeyRepo;
        private readonly ILogger<ApiKeyAuthenticationHandler> _loggerInstance;
        private const string ApiKeyHeaderName = "api-key";

        public ApiKeyAuthenticationHandler(IOptionsMonitor<ApiKeyAuthenticationOptions> options,
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, IApiKeyRepo apiKeyRepo, ILogger<ApiKeyAuthenticationHandler> loggerInstance)
            : base(options, logger, encoder, clock)
        {
            _apiKeyRepo = apiKeyRepo ?? throw new ArgumentNullException(nameof(apiKeyRepo));
            _loggerInstance = loggerInstance;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.TryGetValue(ApiKeyHeaderName, out var apiKeyHeaderValues))
            {
                _loggerInstance.LogInformation("No api key");
                return AuthenticateResult.NoResult();
            }

            string? providedApiKey = apiKeyHeaderValues.FirstOrDefault();

            if (apiKeyHeaderValues.Count == 0 || string.IsNullOrWhiteSpace(providedApiKey))
            {
                _loggerInstance.LogInformation("Api key empty");
                return AuthenticateResult.NoResult();
            }

            ApiKey? existingApiKey = await _apiKeyRepo.GetApiKey(providedApiKey);

            if (existingApiKey != null)
            {
                List<Claim>? claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, existingApiKey.Owner)
                };

                claims.AddRange(existingApiKey.Roles.Select(role => new Claim(ClaimTypes.Role, role)));

                var identity = new ClaimsIdentity(claims, Options.AuthenticationType);
                var identities = new List<ClaimsIdentity> { identity };
                var principal = new ClaimsPrincipal(identities);
                var ticket = new AuthenticationTicket(principal, Options.Scheme);

                _loggerInstance.LogInformation("Valid api key");
                return AuthenticateResult.Success(ticket);
            }

            _loggerInstance.LogInformation("Invalid api key");
            return AuthenticateResult.Fail("Invalid API Key provided.");
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Response.StatusCode = 401;
            Response.ContentType = ProblemDetailsContentType;
            await Response.WriteAsync("Unauthorised");
        }

        protected override async Task HandleForbiddenAsync(AuthenticationProperties properties)
        {
            Response.StatusCode = 403;
            Response.ContentType = ProblemDetailsContentType;
            await Response.WriteAsync("Forbidden");
        }
    }
}
