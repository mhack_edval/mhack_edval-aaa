﻿using System;
using System.Linq;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Models
{
    public partial class ApiRequirement
    {
        public Requirement ToRequirement()
        {
            return new Requirement(PathPart.DecomposePath(Path ?? "", '/').Select(x => x.Value).ToList(), ToPermission(RequiredPermission));
        }

        public Permission ToPermission(ApiPermission? permission)
        {
            return permission switch
            {
                ApiPermission.NoneEnum => Permission.None,
                ApiPermission.ReadEnum => Permission.Read,
                ApiPermission.WriteEnum => Permission.Write,
                ApiPermission.AllEnum => Permission.All,
                null => Permission.None,
                _ => throw new ArgumentOutOfRangeException(nameof(permission), permission, null)
            };
        }
    }
}