﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.Json.Serialization;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.Models
{
    /// <summary>
    /// Represents a part of a path.
    /// e.g. 'teachers' or 'teacher-sets' in /timetable/teachers/teacher-sets
    /// </summary>
    [DebuggerDisplay("{" + nameof(Value) + "}")]
    public class PathPart : IValidatable
    {
        /// <summary>
        /// Chars that can represent the end of URL path.
        /// </summary>
        private static readonly char[] TerminatingChars = {'?', '#'};

        /// <summary>
        /// The value that the path part represents.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Is this a variable path? Variable paths start with '{' and end with '}'.
        /// </summary>
        [JsonIgnore]
        public bool IsVariablePath => Value.StartsWith('{') && Value.EndsWith('}');

        /// <summary>
        /// Warning: if you don't check it is a variable path first, this could crash.
        /// </summary>
        [JsonIgnore]
        public string VariableKey => Value.Substring(1, Value.Length - 2);

        /// <summary>
        /// Should only be used by serializer...
        /// </summary>
        public PathPart()
        {
        }

        public PathPart(string value)   
        {
            if (value.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(value), $"{nameof(value)} cannot be null OR empty.");
            }

            if (value.Contains('/'))
            {
                throw new ArgumentException(
                    $"{nameof(value)} cannot contain the '/' character. It should be a part of a path not a complete path.",
                    nameof(value));
            }

            Value = value;
        }

        public static implicit operator PathPart(string s) => new PathPart(s);
        public static implicit operator string(PathPart p) => p.Value;

        /// <summary>
        /// 'Decomposes' a path into its separate parts.
        /// e.g. /timetable/teachers/subject-teachers into [{timetable}, {teachers}, {subject-teachers}]
        /// </summary>
        public static List<PathPart> DecomposePath(string path, char splitChar = '/')
        {
            if (path.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(path), $"{nameof(path)} cannot be null OR empty.");
            }

            ReadOnlySpan<char> pathSpan = path.AsSpan();
            Span<int> splitIndexes = stackalloc int[64];
            int nextSplitIndex = 0;
            bool terminatingCharPresent = false;

            for (int i = 0; i < pathSpan.Length; i++)
            {
                if (pathSpan[i] == splitChar)
                {
                    splitIndexes[nextSplitIndex++] = i;
                }
                else if (TerminatingChars.Contains(pathSpan[i]))
                {
                    splitIndexes[nextSplitIndex++] = i;
                    terminatingCharPresent = true;
                    break;
                }
            }

            if (!terminatingCharPresent)
            {
                splitIndexes[nextSplitIndex++] = pathSpan.Length;
            }

            List<PathPart> pathParts = new List<PathPart>(6);
            for (int i = 0; i < nextSplitIndex; i++)
            {
                int startIndex = -1;
                if (i > 0)
                {
                    startIndex = splitIndexes[i - 1];
                }

                startIndex++;
                int endIndex = splitIndexes[i];
                int length = endIndex - startIndex;
                if (length == 0)
                {
                    continue;
                }

                pathParts.Add(new PathPart(pathSpan.Slice(startIndex, endIndex - startIndex).ToString()));
            }

            return pathParts;
        }

        protected bool Equals(PathPart other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == this.GetType() && Equals((PathPart) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public (bool valid, string? reason) Validate()
        {
            bool pathIsEmpty = Value.IsNullOrEmpty();
            return pathIsEmpty ? (false, "Path is empty.") : (true, null);
        }
    }
}
