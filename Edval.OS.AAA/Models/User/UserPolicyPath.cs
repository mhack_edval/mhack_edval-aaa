﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.Models.User
{
    /// <summary>
    /// Represents a tree like path structure of permissions the user is granted.
    /// </summary>
    [DebuggerDisplay("{Path}: {GrantedPermission}")]
    public class UserPolicyPath : IValidatable
    {
        /// <summary>
        /// The path part that this policy represents.
        /// </summary>
        public string Path
        {
            get => PathPart.Value;
            set => PathPart = new PathPart(value);
        }

        [IgnoreDataMember, NotMapped]
        public PathPart PathPart { get; private set; }

        /// <summary>
        /// The permission that the user is granted for this path. 
        /// </summary>
        public Permission GrantedPermission { get; set; }

        /// <summary>
        /// The child nodes of this part of the path. 
        /// </summary>
        public List<UserPolicyPath>? Children { get; set; }

        private UserPolicyPath()
        {
        }

        public UserPolicyPath(string path, Permission grantedPermission, List<UserPolicyPath>? children = null)
        {
            Path = path ?? throw new ArgumentNullException(nameof(path));
            GrantedPermission = grantedPermission;
            Children = children;
        }

        public (bool valid, string? reason) Validate()
        {
            if (Path == null)
            {
                return (false, "Path is null.");
            }

            (bool valid, string reason) = PathPart.Validate();
            if (!valid)
            {
                return (false, "Path failed validation: " + reason);
            }

            if (!GrantedPermission.IsDefined())
            {
                return (false, "Invalid granted permission.");
            }

            if (Children != null)
            {
                foreach (UserPolicyPath userPolicyPath in Children)
                {
                    (valid, reason) = userPolicyPath.Validate();

                    if (!valid)
                    {
                        return (false, reason);
                    }
                }
            }

            return (true, null);
        }
    }
}
