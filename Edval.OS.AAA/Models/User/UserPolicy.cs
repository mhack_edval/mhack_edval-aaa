﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.Models.User
{
    [DebuggerDisplay("{Version} {Name}")]
    public class UserPolicy: IValidatable, IVersioned
    {
        /// <summary>
        /// The version of the user policy definition.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// The name of the user policy definition. This is only here
        /// to help with debugging.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The user ID that this policy applies to. This is how we find the user
        /// policy in the DB. Each user will have their own policy.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The tenant ID that this policy applies to. If the user has multiple
        /// tenancies than there should be multiple policies.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// The paths that the user is allowed to access.
        /// </summary>
        public List<UserPolicyPath> AllowedPaths { get; set; }

        public UserPolicy()
        {
            Version = "";
            Name = "";
            UserId = "";
            TenantId = "";
            AllowedPaths = new List<UserPolicyPath>();
        }

        public UserPolicy(string version, string name, string userId, string tenantId, List<UserPolicyPath> allowedPaths)
        {
            Version = version ?? throw new ArgumentNullException(nameof(version));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
            TenantId = tenantId ?? throw new ArgumentNullException(nameof(tenantId));
            AllowedPaths = allowedPaths ?? throw new ArgumentNullException(nameof(allowedPaths));
        }

        private static void AddChildrenToList(UserPolicyPath parent, List<UserPolicyPath> list)
        {
            if (parent.Children == null)
            {
                return;
            }
            
            foreach (UserPolicyPath child in parent.Children)
            {
                list.Add(child);
                AddChildrenToList(child, list);
            }
        }

        /// <summary>
        /// Returns the user policy that corresponds to the passed in <paramref name="pathParts"/>.
        /// </summary>
        /// <param name="pathParts">The path parts to look up.</param>
        public UserPolicyPath? GetUserPolicyCorrespondingToPath(List<PathPart> pathParts)
        {
            List<UserPolicyPath>? node = AllowedPaths;
            for (int i = 0; i < pathParts.Count - 1; i++)
            {
                UserPolicyPath? policyPath = null;
                if (node?.TryGetChild(pathParts[i].Value, out policyPath) == true)
                {
                    node = policyPath?.Children;
                }
                else
                {
                    node = null;
                    break;
                }
            }

            if (node == null)
            {
                return null;
            }

            return node.TryGetChild(pathParts[^1].Value, out UserPolicyPath path) ? path : null;
        }

        public (bool valid, string? reason) Validate()
        {
            if (Version.IsNullOrEmpty())
            {
                return (false, "Version is null or empty.");
            }

            if (Name.IsNullOrEmpty())
            {
                return (false, "Name is null or empty.");
            }

            if (UserId.IsNullOrEmpty())
            {
                return (false, "UserId is null or empty.");
            }

            if (TenantId.IsNullOrEmpty())
            {
                return (false, "TenantId is null or empty.");
            }

            if (AllowedPaths == null)
            {
                return (false, "AllowedPaths was null.");
            }

            foreach (UserPolicyPath userPolicyPath in AllowedPaths)
            {
                (bool valid, string reason) = userPolicyPath.Validate();
                if (!valid)
                {
                    return (false, "Invalid path: " + reason);
                }
            }

            return (true, null);
        }
    }
}
