﻿using System;
using System.Collections.Generic;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Models
{
    public class Request
    {
        public string RequestPath { get; }
        public string AuthToken { get; }
        public HttpMethod HttpMethod { get; }
        public IDictionary<string, string>? Headers { get; }
        public IDictionary<string, string>? QueryStrings { get; }
        public IDictionary<string, string>? PathParameters { get; }

        public Request(string requestPath, string authToken, HttpMethod httpMethod, IDictionary<string, string>? headers = null, 
            IDictionary<string, string>? queryStrings = null, IDictionary<string, string>? pathParameters = null)
        {
            if (requestPath.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(requestPath), $"{nameof(requestPath)} cannot be null OR empty.");
            }

            RequestPath = requestPath;
            AuthToken = authToken;
            HttpMethod = httpMethod;
            Headers = headers;
            QueryStrings = queryStrings;
            PathParameters = pathParameters;
        }

        public Dictionary<string, string> GetAllVariables()
        {
            Dictionary<string, string> outDictionary = new Dictionary<string, string>();

            if (Headers != null)
            {
                foreach (KeyValuePair<string, string> header in Headers)
                {
                    outDictionary.Add(header.Key, header.Value);
                }
            }

            if (QueryStrings != null)
            {
                foreach (KeyValuePair<string, string> queryString in QueryStrings)
                {
                    outDictionary.Add(queryString.Key, queryString.Value);
                }
            }

            if (PathParameters != null)
            {
                foreach (KeyValuePair<string, string> pathParam in PathParameters)
                {
                    outDictionary.Add(pathParam.Key, pathParam.Value);
                }
            }

            return outDictionary;
        }
    }
}
