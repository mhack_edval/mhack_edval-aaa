﻿using System.Collections.Generic;
using System.Linq;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Models
{
    public partial class ApiPathPolicy
    {
        public PathPolicy ToPathPolicy()
        {
            return new PathPolicy(Version ?? "", 
                PathPart.DecomposePath(Path ?? "", '/').Select(x => x.Value).ToList(),
                Enumerable.ToList<PathPolicyVariable>(PathPolicyVariables?.Select(x => x.ToPathPolicyVariable())) ?? new List<PathPolicyVariable>(),
                Enumerable.ToList<PathPolicyMethod>(PathPolicyMethods?.Select(x => x.ToPathPolicyMethod())) ?? new List<PathPolicyMethod>());
        }
    }
}
