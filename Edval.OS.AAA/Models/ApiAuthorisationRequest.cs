﻿using System;
using System.Linq;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Models
{
    public partial class ApiAuthorisationRequest
    {
        public string? TryGetAuthToken()
        {
            string? auth = Cookies
                ?.SingleOrDefault(x => x.Key == "EdvalOS_Auth")
                ?.Value?.ToString();

            if (auth.IsNullOrEmpty())
            {
                auth = Headers
                    ?.SingleOrDefault(x => x.Key == "X-EdvalOS-Auth")
                    ?.Value?.ToString();
            }

            if(auth.IsNullOrEmpty())
            {
                auth = Headers
                    ?.SingleOrDefault(x => x.Key == "EdvalOS_Auth")
                    ?.Value?.ToString();
            }

            return auth;
        }

        public HttpMethod GetHttpMethod()
        {
            return this.HttpMethod switch
            {
                ApiHttpMethod.NoneEnum => Path.HttpMethod.None,
                ApiHttpMethod.GETEnum => Path.HttpMethod.GET,
                ApiHttpMethod.PUTEnum => Path.HttpMethod.PUT,
                ApiHttpMethod.POSTEnum => Path.HttpMethod.POST,
                ApiHttpMethod.DELETEEnum => Path.HttpMethod.DELETE,
                ApiHttpMethod.OPTIONSEnum => Path.HttpMethod.OPTIONS,
                ApiHttpMethod.HEADEnum => Path.HttpMethod.HEAD,
                ApiHttpMethod.CONNECTEnum => Path.HttpMethod.CONNECT,
                ApiHttpMethod.TRACEEnum => Path.HttpMethod.TRACE,
                ApiHttpMethod.PATCHEnum => Path.HttpMethod.PATCH,
                ApiHttpMethod.ANYEnum => Path.HttpMethod.ANY,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}
