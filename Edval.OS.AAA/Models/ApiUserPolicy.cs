﻿using System.Collections.Generic;
using System.Linq;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Models
{
    public partial class ApiUserPolicy
    {
        public UserPolicy ToUserPolicy()
        {
            return new UserPolicy(Version ?? "", Name ?? "", UserId ?? "", TenantId ?? "",
                Enumerable.ToList<UserPolicyPath>(AllowedPaths?.Select(x => x.ToUserPolicyPath())) ?? new List<UserPolicyPath>());
        }
    }
}
