﻿using System;
using System.Collections.Generic;
using System.Linq;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Models
{
    public partial class ApiUserPolicyPath
    {
        public UserPolicyPath ToUserPolicyPath()
        {
            if (StringExtensions.IsNullOrEmpty(Path))
            {
                throw new InvalidOperationException(nameof(Path) + " was null or empty.");
            }

            var paths = PathPart.DecomposePath(Path);
            if (paths.Count == 1)
            {
                return new UserPolicyPath(Path,
                    ToPermission(GrantedPermission),
                    Enumerable.ToList<UserPolicyPath>(Children?.Select(x => x.ToUserPolicyPath())));
            }
            else // manually construct children
            {
                UserPolicyPath parent = new UserPolicyPath(paths[0], Permission.None, new List<UserPolicyPath>());
                var currentParent = parent;
                for (int i = 1; i < paths.Count; i++)
                {
                    bool lastOne = i == paths.Count - 1;

                    UserPolicyPath newPolicyPath = new UserPolicyPath(paths[i], 
                        lastOne ? ToPermission(GrantedPermission) : Permission.None,
                        new List<UserPolicyPath>());

                    currentParent.Children!.Add(newPolicyPath);
                    currentParent = newPolicyPath;
                }

                if (Children != null)
                {
                    currentParent.Children!.AddRange(Enumerable.Select<ApiUserPolicyPath, UserPolicyPath>(Children, x => x.ToUserPolicyPath()));
                }

                return parent;
            }
        }

        private Permission ToPermission(ApiPermission? permission)
        {
            return permission switch
            {
                null => Permission.None,
                ApiPermission.NoneEnum => Permission.None,
                ApiPermission.ReadEnum => Permission.Read,
                ApiPermission.WriteEnum => Permission.Write,
                ApiPermission.AllEnum => Permission.All,
                _ => throw new ArgumentOutOfRangeException(nameof(permission), permission, null)
            };
        }
    }
}