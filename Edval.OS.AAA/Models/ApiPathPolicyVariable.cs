﻿using System;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Models
{
    public partial class ApiPathPolicyVariable
    {
        public PathPolicyVariable ToPathPolicyVariable()
        {
            return new PathPolicyVariable(Key, ToLocation(Location), RequestKey);
        }

        private PathPolicyVariableLocation ToLocation(LocationEnum? location)
        {
            return location switch
            {
                LocationEnum.NoneEnum => PathPolicyVariableLocation.None,
                LocationEnum.HeaderEnum => PathPolicyVariableLocation.Header,
                LocationEnum.QueryEnum => PathPolicyVariableLocation.Query,
                LocationEnum.PathParameterEnum => PathPolicyVariableLocation.PathParameter,
                null => PathPolicyVariableLocation.None,
                _ => throw new ArgumentOutOfRangeException(nameof(location), location, null)
            };
        }
    }
}