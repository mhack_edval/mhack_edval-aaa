﻿using System;
using System.Linq;
using Edval.OS.AAA.Models.Path;

namespace Edval.OS.AAA.Models
{
    public partial class ApiPathPolicyMethod
    {
        public PathPolicyMethod ToPathPolicyMethod()
        {
            return new PathPolicyMethod(ToHttpMethod(HttpMethod), 
                Enumerable.Select<ApiRequirement, Requirement>(Requirements, x => x.ToRequirement()).ToList(), 
                AllowPublicAccess ?? false);
        }

        private HttpMethod ToHttpMethod(ApiHttpMethod? apiMethod)
        {
            return apiMethod switch
            {
                null => Path.HttpMethod.None,
                ApiHttpMethod.NoneEnum => Path.HttpMethod.None,
                ApiHttpMethod.GETEnum => Path.HttpMethod.GET,
                ApiHttpMethod.PUTEnum => Path.HttpMethod.PUT,
                ApiHttpMethod.POSTEnum => Path.HttpMethod.POST,
                ApiHttpMethod.DELETEEnum => Path.HttpMethod.DELETE,
                ApiHttpMethod.OPTIONSEnum => Path.HttpMethod.OPTIONS,
                ApiHttpMethod.HEADEnum => Path.HttpMethod.HEAD,
                ApiHttpMethod.CONNECTEnum => Path.HttpMethod.CONNECT,
                ApiHttpMethod.TRACEEnum => Path.HttpMethod.TRACE,
                ApiHttpMethod.PATCHEnum => Path.HttpMethod.PATCH,
                ApiHttpMethod.ANYEnum => Path.HttpMethod.ANY,
                _ => throw new ArgumentOutOfRangeException(nameof(apiMethod), apiMethod, null)
            };
        }
    }
}