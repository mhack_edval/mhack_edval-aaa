﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json.Serialization;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.Models.Path
{
    [DebuggerDisplay("{HttpMethod}: {Requirements.Count} requirement(s)")]
    public class PathPolicyMethod: IValidatable
    {
        /// <summary>
        /// The HttpMethod this policy applies to.
        /// </summary>
        public HttpMethod HttpMethod { get; set; }

        /// <summary>
        /// The requirements of this policy. All of them must be satisfied for the request to be granted.
        /// </summary>
        public List<Requirement> Requirements { get; set; }

        /// <summary>
        /// Allow anyone to access this.
        /// </summary>
        public bool AllowPublicAccess { get; set; }

        private PathPolicyMethod()
        {
        }

        public PathPolicyMethod(HttpMethod method, List<Requirement> requirements, bool allowPublicAccess)
        {
            HttpMethod = method;
            Requirements = requirements;
            AllowPublicAccess = allowPublicAccess;
        }

        public (bool valid, string reason) Validate()
        {
            if (!this.HttpMethod.IsDefined())
            {
                return (false, $"{nameof(HttpMethod)} is invalid.");
            }

            if (HttpMethod == HttpMethod.None)
            {
                return (false, $"{nameof(HttpMethod)} is not set.");
            }

            if (AllowPublicAccess)
            {
                if (Requirements != null && Requirements.Count > 0)
                {
                    return (false, $"{nameof(AllowPublicAccess)} is true which means there has to be 0 requirements.");
                }

                return (true, null);
            }

            if (Requirements == null || Requirements.Count == 0)
            {
                return (false, $"{nameof(Requirements)} is not set.");
            }

            foreach (Requirement requirement in Requirements)
            {
                (bool valid, string reason) = requirement.Validate();
                if (!valid)
                {
                    return (false, $"Invalid requirement '{requirement.PathParts.ComposePath()}': " + reason);
                }
            }

            return (true, null);
        }
    }

    [DebuggerDisplay("{Path}: {RequiredPermission}")]
    public class Requirement : IValidatable
    {
        /// <summary>
        /// The path of the requirement.
        /// 
        /// If part of the path is present and has the required
        /// permission level that is acceptable too.
        /// </summary>
        [JsonIgnore]
        public List<string> Path
        {
            get => PathParts.Select(x => x.Value).ToList();
            set => PathParts = value.Select(x => new PathPart(x)).ToList();
        }

        public List<PathPart> PathParts { get; set; }

        /// <summary>
        /// The permission level that must be present (or higher).
        /// </summary>
        public Permission RequiredPermission { get; set; }

        private Requirement()
        {
        }

        public Requirement(List<string> path, Permission requiredPermission)
        {
            Path = path;
            RequiredPermission = requiredPermission;
        }

        public (bool valid, string? reason) Validate()
        {
            if (Path == null || Path.Count == 0)
            {
                return (false, "Path is not present.");
            }

            foreach (PathPart pathPart in Path)
            {
                (bool valid, string reason) = pathPart.Validate();
                if (!valid)
                {
                    return (false, $"Path part '{pathPart.Value}' is not valid: " + reason);
                }
            }

            if (!RequiredPermission.IsDefined())
            {
                return (false, "Invalid required permission");
            }

            return (true, null);
        }
    }
}
