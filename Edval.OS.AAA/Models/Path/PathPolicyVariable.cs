﻿using System.Diagnostics;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.Models.Path
{
    [DebuggerDisplay("Key: {PolicyKey}, RequestKey: {RequestKey}, Location: {Location}")]
    public class PathPolicyVariable : IValidatable
    {
        /// <summary>
        /// The key that is used internally for the policy.
        /// </summary>
        public string PolicyKey { get; set; }
        
        /// <summary>
        /// Where should we look to find the value in the request?
        /// </summary>
        public PathPolicyVariableLocation Location { get; set; }

        /// <summary>
        /// What is the variable called in the request.
        /// e.g. if it is located in the header this is the header name to lookup.
        /// </summary>
        public string RequestKey { get; set; }

        public const int MaxKeyLength = 32;

        private PathPolicyVariable()
        {
        }

        public PathPolicyVariable(string policyKey, PathPolicyVariableLocation location, string requestKey)
        {
            PolicyKey = policyKey;
            Location = location;
            RequestKey = requestKey;
        }

        public (bool valid, string? reason) Validate()
        {
            if (PolicyKey.IsNullOrEmpty())
            {
                return (false, $"{nameof(PolicyKey)} is null or empty.");
            }

            if (PolicyKey.Length > MaxKeyLength)
            {
                return (false, $"{nameof(PolicyKey)} length is too long.");
            }

            if (Location == PathPolicyVariableLocation.None)
            {
                return (false, $"{nameof(Location)} is set to None.");
            }

            if (!Location.IsDefined())
            {
                return (false, $"{nameof(Location)} is invalid.");
            }

            if (RequestKey.IsNullOrEmpty())
            {
                return (false, $"{nameof(RequestKey)} is null or empty.");
            }

            if (RequestKey.Length > MaxKeyLength)
            {
                return (false, $"{nameof(RequestKey)} length is too long.");
            }

            return (true, null);
        }
    }

    public enum PathPolicyVariableLocation
    {
        None,
        Header,
        Query,
        PathParameter
    }
}
