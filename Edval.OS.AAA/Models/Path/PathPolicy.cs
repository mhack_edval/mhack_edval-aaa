﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Utils;

namespace Edval.OS.AAA.Models.Path
{
    public class PathPolicy: IValidatable, IVersioned
    {
        /// <summary>
        /// Must call PrepareForDb() before using.
        /// </summary>
        public string Key { get; set; }
        
        /// <summary>
        /// The version of the policy definition.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// The path of the request. This should match the URL of the request and is
        /// how the authoriser finds the correct policy.
        /// </summary>
        [JsonIgnore]
        public List<string> Path
        {
            get => PathParts.Select(x => x.Value).ToList();
            set => PathParts = value.Select(x => new PathPart(x)).ToList();
        }

        public List<PathPart> PathParts { get; set; }

        /// <summary>
        /// PathPolicy variables definition. Shows us where to look for variables in the request.
        /// </summary>
        public List<PathPolicyVariable> PathPolicyVariables { get; set; }

        /// <summary>
        /// Supported methods on the policy and the requirements to allow the request through.
        /// </summary>
        public List<PathPolicyMethod> PathPolicyMethods { get; set; }

        public PathPolicy()
        {
            Version = "";
            Path = new List<string>();
            PathPolicyVariables = new List<PathPolicyVariable>();
            PathPolicyMethods = new List<PathPolicyMethod>();
        }

        public PathPolicy(string version, List<string> path,
            List<PathPolicyVariable> pathPolicyVariables,
            List<PathPolicyMethod> pathPolicyMethods)
        {
            if (version.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(version), $"{nameof(version)} cannot be null OR empty.");
            }

            Version = version;
            Path = path ?? throw new ArgumentNullException(nameof(path));
            PathPolicyVariables = pathPolicyVariables ?? throw new ArgumentNullException(nameof(pathPolicyVariables));
            PathPolicyMethods = pathPolicyMethods ?? throw new ArgumentNullException(nameof(pathPolicyMethods));
        }

        /// <summary>
        /// Must be called before sending to the db.
        /// </summary>
        public void PrepareForDb()
        {
            Key = PathParts.ComposePath('/');
            if (Key.IsNullOrEmpty())
            {
                throw new InvalidOperationException(nameof(Key) + " must have a value.");
            }
        }

        public PathPolicyMethod? FindPathPolicyMethod(HttpMethod httpMethod)
        {
            // search for an exact match before accepting an ANY PathPolicyMethod
            PathPolicyMethod anyMethod = null;
            for (int i = 0; i < PathPolicyMethods.Count; i++)
            {
                if (PathPolicyMethods[i].HttpMethod == httpMethod)
                {
                    return PathPolicyMethods[i];
                }

                if (PathPolicyMethods[i].HttpMethod == HttpMethod.ANY)
                {
                    // keep track of this 
                    anyMethod = PathPolicyMethods[i]; 
                }
            }

            return anyMethod;
        }

        public (bool valid, string? reason) Validate()
        {
            if (Version.IsNullOrEmpty())
            {
                return (false, "Version is empty.");
            }

            if (Path == null || Path.Count == 0)
            {
                return (false, "No path present");
            }

            if (Path[0]?.ToLowerInvariant().StartsWith("http") == true)
            {
                return (false, "You should not use domain names in path policies.");
            }

            foreach (PathPart part in Path)
            {
                (bool valid, string reason) = part.Validate();
                if (!valid)
                {
                    return (false, $"Path part '{part.Value}' is not valid - reason: " + reason);
                }

                if (part.Value == "localhost")
                {
                    return (false, "You should not set path policies for localhost");
                }

                if (part.Value == "com")
                {
                    return (false, "You should not set path policies for URLs. (don't use .com as a path part)");
                }
            }

            if (PathPolicyVariables != null)
            {
                foreach (PathPolicyVariable pathPolicyVariable in PathPolicyVariables)
                {
                    (bool valid, string reason) = pathPolicyVariable.Validate();
                    if (!valid)
                    {
                        return (false, $"Path policy variable '{pathPolicyVariable.PolicyKey}' is not valid - reason: " + reason);
                    }
                }
            }

            if (PathPolicyMethods == null || PathPolicyMethods.Count == 0)
            {
                return (false, "Path policy methods are empty.");
            }

            foreach (PathPolicyMethod method in PathPolicyMethods)
            {
                (bool valid, string reason) = method.Validate();
                if (!valid)
                {
                    return (false, $"Path policy method {method.HttpMethod} is invalid: " + reason);
                }
            }

            return (true, null);
        }
    }
}
