﻿namespace Edval.OS.AAA.Models.Path
{
    public enum HttpMethod
    {
        None,
        GET,
        PUT,
        POST,
        DELETE,
        OPTIONS,
        HEAD,
        CONNECT,
        TRACE,
        PATCH,
        ANY
    }
}