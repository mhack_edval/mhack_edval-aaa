﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;
using SharpYaml.Serialization;

namespace Edval.OS.AAA.Models
{
    public class PolicyBuilder
    {
        private const string DefaultStringVal = "";

        /// <summary>
        /// Build a user policy from YAML. Not really recommended for production - used for testing.
        /// </summary>
        public UserPolicy BuildUserPolicyFromYaml(string yaml)
        {
            if (yaml.IsNullOrEmpty())
            {
                return new UserPolicy();
            }

            Serializer serializer = new Serializer();
            Dictionary<object, object> deserialisedObject = serializer.Deserialize<Dictionary<object, object>>(yaml);

            UserPolicy resultPolicy = null;

            if (deserialisedObject.TryGetValue("policy", out object objPolicy) && objPolicy is Dictionary<object, object> policy)
            {
                if (!policy.TryGetValue("version", out object objVersion) || !(objVersion is string version))
                {
                    version = DefaultStringVal;
                }

                if (!policy.TryGetValue("name", out object objName) || !(objName is string name))
                {
                    name = DefaultStringVal;
                }

                if (!policy.TryGetValue("user-id", out object objUserId) || !(objUserId is string userId))
                {
                    userId = DefaultStringVal;
                }

                if (!policy.TryGetValue("tenant-id", out object objTenantId) || !(objTenantId is string tenantId))
                {
                    tenantId = DefaultStringVal;
                }

                resultPolicy = new UserPolicy(version, name, userId, tenantId, new List<UserPolicyPath>());

                List<UserPolicyPath> userPolicies = resultPolicy.AllowedPaths;
                if (policy.TryGetValue("allow", out object objAllow))
                {
                    if (objAllow is string allowString)
                    {
                        userPolicies.Add(new UserPolicyPath(new PathPart("*"), allowString.ParsePermission()));
                    }
                    else if (objAllow is Dictionary<object, object> allowMap)
                    {
                        RecursivelyParseDictionary(allowMap, userPolicies);
                    }
                }
            }

            return resultPolicy ?? new UserPolicy();
        }

        private void RecursivelyParseDictionary([NotNull] Dictionary<object, object> allowMap, List<UserPolicyPath> parentList)
        {
            foreach (KeyValuePair<object, object> keyValuePair in allowMap)
            {
                string path;
                if (keyValuePair.Key is string keyString)
                {
                    if (keyString.IsNullOrEmpty())
                    {
                        Debug.Assert(false, "Key is null or empty string.");
                        continue;
                    }

                    path = keyString;
                }
                else if (keyValuePair.Key is null)
                {
                    Debug.Assert(false,"Null key.");
                    continue;
                }
                else
                {
                    Debug.Assert(false, $"Unknown key type: {keyValuePair.Key.GetType()}");
                    continue;
                }

                if (keyValuePair.Value is string valueString)
                {
                    parentList.Add(new UserPolicyPath(new PathPart(path), valueString.ParsePermission()));
                }
                else if (keyValuePair.Value is Dictionary<object, object> valueMap)
                {
                    UserPolicyPath thisPolicyPath = new UserPolicyPath(new PathPart(path), Permission.None, new List<UserPolicyPath>());
                    parentList.Add(thisPolicyPath);
                    RecursivelyParseDictionary(valueMap, thisPolicyPath.Children);
                }
                else if (keyValuePair.Value is List<object> valueList)
                {
                    UserPolicyPath thisPolicyPath = new UserPolicyPath(new PathPart(path), Permission.None, new List<UserPolicyPath>());
                    parentList.Add(thisPolicyPath);
                    for (int i = 0; i < valueList.Count; i++)
                    {
                        if (valueList[i] is Dictionary<object, object> map)
                        {
                            RecursivelyParseDictionary(map, thisPolicyPath.Children);
                        }
                        else
                        {
                            Debug.Assert(false, "Unknown list type");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds a path policy from YAML not really recommended for production - used for testing.
        /// </summary>
        public PathPolicy BuildPathPolicyFromYaml(string yaml)
        {
            Serializer serializer = new Serializer();
            Dictionary<object, object> deserialisedObject = serializer.Deserialize<Dictionary<object, object>>(yaml);

            PathPolicy resultPolicy = null;

            if (deserialisedObject.TryGetValue("policy", out object objPolicy) && objPolicy is Dictionary<object, object> policy)
            {
                if (!policy.TryGetValue("version", out object objVersion) || !(objVersion is string version))
                {
                    version = DefaultStringVal;
                }

                if (!policy.TryGetValue("path", out object objPath) || !(objPath is string path))
                {
                    path = DefaultStringVal;
                }

                resultPolicy = new PathPolicy(version, PathPart.DecomposePath(path).Select(x => x.Value).ToList(), new List<PathPolicyVariable>(), new List<PathPolicyMethod>());
                List<PathPolicyVariable> policyVariables = resultPolicy.PathPolicyVariables;
                if (policy.TryGetValue("variables", out object objVariables))
                {
                    if (objVariables is List<object> variablesList)
                    {
                        for (int i = 0; i < variablesList.Count; i++)
                        {
                            if (!(variablesList[i] is Dictionary<object, object> variableObject))
                            {
                                continue;
                            }

                            if (!variableObject.TryGetValue("key", out object objKey) || !(objKey is string key))
                            {
                                continue;
                            }

                            if (!variableObject.TryGetValue("location", out object objLocation) || !(objLocation is string location))
                            {
                                continue;
                            }

                            policyVariables.Add(new PathPolicyVariable(key, location.ParseLocation(), key));
                        }
                    } 
                }

                List<PathPolicyMethod> policyMethodList = resultPolicy.PathPolicyMethods;
                if (policy.TryGetValue("allow", out object objAllow))
                {
                    if (objAllow is List<object> allowList)
                    {
                        for (int i = 0; i < allowList.Count; i++)
                        {
                            if (!(allowList[i] is Dictionary<object, object> allowObject))
                            {
                                continue;
                            }

                            if (!allowObject.TryGetValue("method", out object objMethod) || !(objMethod is string method))
                            {
                                continue;
                            }

                            if (!allowObject.TryGetValue("requires", out object objRequires) || !(objRequires is List<object> requirementsList))
                            {
                                continue;
                            }

                            bool publicAccess = false;
                            {
                                if (allowObject.TryGetValue("allowPublicAccess", out object objAllowPub) && objAllowPub is bool allowPub)
                                {
                                    publicAccess = allowPub;
                                }
                            }

                            PathPolicyMethod pathPolicyMethod = new PathPolicyMethod(method.ParseHttpMethod(), new List<Requirement>(), publicAccess);
                            policyMethodList.Add(pathPolicyMethod);
                            for (int j = 0; j < requirementsList.Count; j++)
                            {
                                if (!(requirementsList[j] is Dictionary<object, object> requirementObject))
                                {
                                    continue;
                                }

                                if (!requirementObject.TryGetValue("path", out object objRequirePath) || !(objRequirePath is string requirePath))
                                {
                                    continue;
                                }

                                if (!requirementObject.TryGetValue("level", out object objRequirePermission) || !(objRequirePermission is string requirePermission))
                                {
                                    continue;
                                }

                                pathPolicyMethod.Requirements.Add(new Requirement(PathPart.DecomposePath(requirePath, '.').Select(x => x.Value).ToList(), requirePermission.ParsePermission()));
                            }
                        }
                    }
                }
            }

            return resultPolicy ?? new PathPolicy();
        }
    }
}
