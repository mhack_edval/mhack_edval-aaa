﻿namespace Edval.OS.AAA.Models
{
    public enum Permission
    {
        /// <summary>
        /// No permission. Default.
        /// </summary>
        None = 0,

        /// <summary>
        /// Read permissions.
        /// </summary>
        Read = 1,

        /// <summary>
        /// Read and write permissions.
        /// </summary>
        Write = 2,

        /// <summary>
        /// All permissions.
        /// </summary>
        All = 3
    }
}
