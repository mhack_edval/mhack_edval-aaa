﻿using System;
using System.Collections.Generic;
using System.Linq;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Services
{
    /// <summary>
    /// All the logic for checking if requests should be allowed.
    /// </summary>
    public class PolicyEnforcer
    {
        /// <summary>
        /// Compares the UserPolicy, PathPolicy and request then returns true if access is allowed.
        /// All PathPolicy permissions must be satisfied in order for the request to be granted.
        /// </summary>
        public bool IsAccessAllowed(UserPolicy? userPolicy, PathPolicyMethod? pathPolicyMethod, Dictionary<string, string> requestVariableValues)
        {
            if (userPolicy == null)
            {
                return false;
            }

            if (pathPolicyMethod == null)
            {
                return false;
            }

            if (!requestVariableValues.ContainsKey("TenantId"))
            {
                requestVariableValues.Add("TenantId", userPolicy.TenantId);
            }
            
            if (requestVariableValues.TryGetValue("TenantId", out string? tenantId) && tenantId != userPolicy.TenantId)
            {
                Console.WriteLine("Invalid tenant ID.");
                return false;
            }

            for (int requirementIndex = 0; requirementIndex < pathPolicyMethod.Requirements.Count; requirementIndex++)
            {
                Requirement requirement = pathPolicyMethod.Requirements[requirementIndex];
                List<PathPart> requiredPath = requirement.PathParts.ToList();
                for (int requirementPathIndex = 0; requirementPathIndex < requiredPath.Count; requirementPathIndex++)
                {
                    PathPart pathPart = requiredPath[requirementPathIndex];
                    if (pathPart.IsVariablePath)
                    {
                        if (requestVariableValues.TryGetValue(pathPart.VariableKey, out string? value) && !value.IsNullOrEmpty())
                        {
                            requiredPath[requirementPathIndex] = new PathPart(value);
                        }
                        else
                        {
                            // TODO should this be an error?
                            // throw new Exception($"Requested variable '{pathPart.VariableKey}' was not present.");
                        }
                    }
                }

                if (!HasRequiredPermission(requirement.RequiredPermission, userPolicy.AllowedPaths, requiredPath, 0))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Recursive tree DFS for the required permission. Exits early if the path doesn't match. 
        /// </summary>
        private bool HasRequiredPermission(Permission requiredPermission, List<UserPolicyPath>? paths, List<PathPart> pathToMatch, int currentDepth)
        {
            if (paths == null)
            {
                return false;
            }

            for (int policyIndex = 0; policyIndex < paths.Count; policyIndex++)
            {
                UserPolicyPath policyPath = paths[policyIndex];

                if (policyPath.PathPart.IsVariablePath)
                {
                    if (policyPath.Children == null)
                    {
                        continue;
                    }

                    foreach (UserPolicyPath policyPathChild in policyPath.Children)
                    {
                        string pathValue = policyPathChild.PathPart.Value;
                        if (pathValue != pathToMatch[currentDepth].Value)
                        {
                            // path doesn't match
                            continue;
                        }

                        if (policyPathChild.GrantedPermission >= requiredPermission)
                        {
                            // subset of the path with required permission
                            // e.g. we're looking for timetable.classes
                            // but the user has blanket access to timetable: '*'
                            return true;
                        }
                    }
                }
                else
                {
                    string pathValue = policyPath.PathPart.Value;

                    if (pathValue != pathToMatch[currentDepth].Value)
                    {
                        // path doesn't match
                        continue;
                    }

                    if (policyPath.GrantedPermission >= requiredPermission)
                    {
                        // subset of the path with required permission
                        // e.g. we're looking for timetable.classes
                        // but the user has blanket access to timetable: '*'
                        return true;
                    }

                    if (currentDepth == pathToMatch.Count - 1)
                    {
                        // end of the path lookup
                        continue;
                    }

                    if (HasRequiredPermission(requiredPermission, policyPath.Children, pathToMatch, currentDepth + 1))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
