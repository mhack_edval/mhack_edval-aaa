﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Edval.OS.AAA.Database;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.JWT;
using Edval.OS.AAA.JWT.PublicKeys;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;
using JsonWebToken;

namespace Edval.OS.AAA.Services
{
    public class RequestAuthoriser<TPublicKeyType> where TPublicKeyType : class, IPublicKeys, new()
    {
        private readonly string _validTokenIssuer;
        private readonly string _keyUrl;
        private readonly IAuthRepo _db;
        private readonly PolicyEnforcer _policyEnforcer;

        public RequestAuthoriser(string validTokenIssuer, string keyUrl, IAuthRepo authRepo, PolicyEnforcer policyEnforcer)
        {
            if(validTokenIssuer.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(validTokenIssuer));
            }

            if (keyUrl.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(keyUrl));
            }

            _validTokenIssuer = validTokenIssuer;
            _keyUrl = keyUrl;
            _db = authRepo;
            _policyEnforcer = policyEnforcer;
        }

        private static (bool success, Jwt? jwt, UserPolicy? userPolicy) InvalidResult => (false, null, null);

        public async Task<(bool success, Jwt? jwt, UserPolicy? userPolicy)> IsRequestAuthorised(Request request)
        {
            PathPolicy? pathPolicy = await GetPathPolicy(request.RequestPath);
            if (pathPolicy == null)
            {
                Console.WriteLine("Couldn't find path policy.");
                return InvalidResult;
            }

            PathPolicyMethod? pathPolicyMethod = pathPolicy.FindPathPolicyMethod(request.HttpMethod);

            if (pathPolicyMethod?.AllowPublicAccess == true)
            {
                Console.WriteLine("Public access.");
                return (true, null, null);
            }

            if (request.AuthToken.IsNullOrEmpty())
            {
                Console.WriteLine("No auth token.");
                return InvalidResult;
            }

            Console.WriteLine("Validating token"); 
            JwtHandler handler = new JwtHandler(_validTokenIssuer, await PublicKeyCache<TPublicKeyType>.GetKeys(_keyUrl));
            var result = handler.TryValidateToken(request.AuthToken, SignatureAlgorithm.RsaSha256, out Jwt? token);

            if (result == JwtHandler.Response.InvalidOrUnknownPublicKey)
            {
                // try again
                Console.WriteLine("InvalidOrUnknownPublicKey - trying again");
                await PublicKeyCache<CognitoPublicKeys>.ClearCache();
                handler = new JwtHandler(_validTokenIssuer, await PublicKeyCache<TPublicKeyType>.GetKeys(_keyUrl));
                result = handler.TryValidateToken(request.AuthToken, SignatureAlgorithm.RsaSha256, out token);
            }

            bool tokenValid = result == JwtHandler.Response.Success;
            if (!tokenValid || token == null)
            {
                Console.WriteLine("Token invalid - " + result);
                return InvalidResult;
            }

            if (token.Subject.IsNullOrEmpty())
            {
                Console.WriteLine("Token had no subject.");
                return InvalidResult;
            }

            UserPolicy? userPolicy = await _db.GetUserPolicy(token.Subject);

            if (userPolicy == null)
            {
                Console.WriteLine("Can't find user.");
                return InvalidResult;
            }

            bool allow = _policyEnforcer.IsAccessAllowed(userPolicy, pathPolicyMethod, request.GetAllVariables());
            Console.WriteLine("PolicyEnforcer result: " + (allow ? "allowed" : "denied"));
            return allow ? (true, token, userPolicy) : InvalidResult;
        }

        private async Task<PathPolicy?> GetPathPolicy(string path)
        {
            // TODO add caching??

            List<PathPart> pathParts = PathPart.DecomposePath(path);
            StringBuilder pathToSearch = new StringBuilder(64);
            int limit = pathParts.Count;

            while (limit > 0)
            {
                pathToSearch.Length = 0;
                for (int i = 0; i < limit; i++)
                {
                    pathToSearch.Append(pathParts[i].Value);
                    pathToSearch.Append('/');
                }

                pathToSearch.Length -= 1; // remove last '/'

                PathPolicy? policy = await _db.GetPathPolicy(pathToSearch.ToString());
                if (policy != null)
                {
                    return policy;
                }
                
                limit--;
            }

            return null;
        }
    }
}
