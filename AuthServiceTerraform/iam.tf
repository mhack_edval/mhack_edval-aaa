resource "aws_iam_role" "authservice_role" {
  name               = "AuthServiceECSRole"
  assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Principal": {
				"Service": "ecs-tasks.amazonaws.com"
			},
			"Action": "sts:AssumeRole"
		}
	]
}
EOF
}

resource "aws_iam_policy" "write_logs_policy" {
  name        = "AuthServiceWriteLogsPolicy"
  description = "Allows auth service to write to logs."
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:CreateLogStream",
                "logs:CreateLogGroup",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams"
            ],
            "Resource": [
                "arn:aws:logs:*:*:*"
            ],
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "access_to_cognito_policy" {
  name        = "AuthServiceCognitoAccessPolicy"
  description = "Allows the AuthService to access Cognito."
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "cognito-idp:AdminInitiateAuth",
                "cognito-idp:AdminCreateUser",
                "cognito-idp:AdminSetUserPassword",
                "cognito-idp:AdminUpdateUserAttributes",
                "cognito-idp:AdminRespondToAuthChallenge",
                "cognito-idp:AdminGetUser",
                "cognito-idp:AdminConfirmSignUp"
            ],
            "Resource": "arn:aws:cognito-idp:ap-southeast-2:776995227935:userpool/ap-southeast-2_KMqvOg8Dq"
        }
    ]
}

EOF
}

resource "aws_iam_role_policy_attachment" "attach_logs_policy" {
  role       = aws_iam_role.authservice_role.name
  policy_arn = aws_iam_policy.write_logs_policy.arn
}

resource "aws_iam_role_policy_attachment" "attach_ecr_read_policy" {
  role       = aws_iam_role.authservice_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "attach_cognito_policy" {
  role       = aws_iam_role.authservice_role.name
  policy_arn = aws_iam_policy.access_to_cognito_policy.arn
}