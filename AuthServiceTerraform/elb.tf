resource "aws_lb_target_group" "auth_service_target_group" {
  name                 = "auth-target-group"
  deregistration_delay = 60
  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    port                = "traffic-port"
    protocol            = "TCP"
    unhealthy_threshold = 3
  }
  port        = 5125
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = "vpc-05a550f7ec0dba7b1"
}

resource "aws_lb_listener" "auth_service_nlb_listener" {
  load_balancer_arn = "arn:aws:elasticloadbalancing:ap-southeast-2:776995227935:loadbalancer/net/edval-os-nlb/20464034076b3320"
  port              = 5125
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.auth_service_target_group.arn
  }
}