terraform {
  backend "s3" {
    bucket         = "edvalos-terraform-state"
    key            = "terraform/state/auth"
    region         = "ap-southeast-2"
    dynamodb_table = "edvalos-terraform-lock"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "ap-southeast-2"
}