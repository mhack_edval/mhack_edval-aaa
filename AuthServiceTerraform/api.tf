resource "aws_api_gateway_resource" "aaa_resource" {
  rest_api_id = var.rest_api_id
  parent_id   = var.rest_api_root_id
  path_part   = "aaa"
}

resource "aws_api_gateway_method" "aaa_method" {
  rest_api_id   = var.rest_api_id
  resource_id   = aws_api_gateway_resource.aaa_resource.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "aaa_integration" {
  rest_api_id             = var.rest_api_id
  resource_id             = aws_api_gateway_resource.aaa_resource.id
  connection_type         = "VPC_LINK"
  connection_id           = var.rest_api_vpc_link
  http_method             = "ANY"
  integration_http_method = "ANY"
  passthrough_behavior    = "WHEN_NO_MATCH"
  type                    = "HTTP_PROXY"
  uri                     = var.nlb_address
  depends_on = [
    aws_api_gateway_method.aaa_method
  ]
}

resource "aws_api_gateway_resource" "aaa_proxy_resource" {
  rest_api_id = var.rest_api_id
  parent_id   = aws_api_gateway_resource.aaa_resource.id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "aaa_proxy_method" {
  rest_api_id   = var.rest_api_id
  resource_id   = aws_api_gateway_resource.aaa_proxy_resource.id
  http_method   = "ANY"
  authorization = "NONE"
  request_parameters = {
    "method.request.path.proxy" : true
  }
}

resource "aws_api_gateway_integration" "aaa_proxy_integration" {
  rest_api_id             = var.rest_api_id
  resource_id             = aws_api_gateway_resource.aaa_proxy_resource.id
  connection_type         = "VPC_LINK"
  connection_id           = var.rest_api_vpc_link
  http_method             = "ANY"
  integration_http_method = "ANY"
  passthrough_behavior    = "WHEN_NO_MATCH"
  type                    = "HTTP_PROXY"
  uri                     = "${var.nlb_address}/{proxy}"
  request_parameters = {
    "integration.request.path.proxy" = "method.request.path.proxy"
  }
  depends_on = [
    aws_api_gateway_method.aaa_proxy_method
  ]
}