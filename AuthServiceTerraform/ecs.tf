resource "aws_ecs_task_definition" "auth_task_def" {
  container_definitions = templatefile("task-definitions/auth-service.json", {
    region         = var.region
    account_id     = var.account_id
    auth_repo_name = aws_ecr_repository.edval_os_auth.name
  })

  cpu                      = 256
  execution_role_arn       = aws_iam_role.authservice_role.arn
  family                   = "AuthService"
  memory                   = 512
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  task_role_arn            = aws_iam_role.authservice_role.arn
  volume {
    name = "AuthServiceVolume"
  }
}

resource "aws_ecs_service" "auth_service" {
  cluster = "arn:aws:ecs:ap-southeast-2:776995227935:cluster/EdvalOSCluster"
  deployment_controller {
    type = "ECS"
  }
  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 0
  desired_count                      = 2
  health_check_grace_period_seconds  = 0
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"
  task_definition                    = aws_ecs_task_definition.auth_task_def.arn
  name                               = "AuthService"
  network_configuration {
    assign_public_ip = false
    security_groups  = ["sg-0585ad1f8a67d484d"]
    subnets          = ["subnet-081d83afa859a8900", "subnet-0f1c4a61deea7b4f4"]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.auth_service_target_group.arn
    container_name   = "AuthService"
    container_port   = 5125
  }
}