variable "region" {
  type    = string
  default = "ap-southeast-2"
}

variable "account_id" {
  type    = string
  default = "776995227935"
}

variable "rest_api_id" {
  type    = string
  default = "rp5mv7rmka"
}

variable "rest_api_root_id" {
  type    = string
  default = "0ubzv1uskf"
}

variable "rest_api_vpc_link" {
  type    = string
  default = "v6zqv5"
}

variable "nlb_address" {
  type    = string
  default = "http://edval-os-nlb-20464034076b3320.elb.ap-southeast-2.amazonaws.com:5125"
}