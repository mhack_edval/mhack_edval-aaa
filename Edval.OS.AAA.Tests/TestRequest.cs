﻿using System.Collections.Generic;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestRequest
    {
        [TestMethod]
        public void GetAllVariables()
        {
            Request request = new Request("test/here/hello", "none", HttpMethod.GET, 
                new Dictionary<string, string>()
                {
                    { "header1", "header1Value"},
                    {"header2", "header2Value"}
                },
                new Dictionary<string, string>()
                {
                    { "query1", "query1Value"},
                    {"query2", "query2Value"}
                }, 
                new Dictionary<string, string>()
                {
                    {"path1", "path1Value"},
                    {"path2", "path2Value"}
                });

            var allVariables = request.GetAllVariables();
            allVariables["header1"].Should().Be("header1Value");
            allVariables["header2"].Should().Be("header2Value");
            allVariables["query1"].Should().Be("query1Value");
            allVariables["query2"].Should().Be("query2Value");
            allVariables["path1"].Should().Be("path1Value");
            allVariables["path2"].Should().Be("path2Value");
            allVariables.Count.Should().Be(6);

            request.RequestPath.Should().Be("test/here/hello");
            request.AuthToken.Should().Be("none");
            request.HttpMethod.Should().Be(HttpMethod.GET);
        }

        [TestMethod]
        public void GetAllVariablesEmpty()
        {
            Request request = new Request("hey", "none", HttpMethod.PUT);
            request.RequestPath.Should().Be("hey");
            request.AuthToken.Should().Be("none");
            request.HttpMethod.Should().Be(HttpMethod.PUT);
            request.GetAllVariables().Should().BeEmpty();
        }
    }
}
