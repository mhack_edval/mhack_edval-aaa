﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Edval.OS.AAA.Database;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;

namespace Edval.OS.AAA.Tests
{
    public class DummyInMemoryAuthRepo : IAuthRepo
    {
        private readonly Dictionary<string, UserPolicy> _userPolicies = new Dictionary<string, UserPolicy>();
        private readonly Dictionary<string, PathPolicy> _pathPolicies = new Dictionary<string, PathPolicy>();

        public Task<UserPolicy> GetUserPolicy(string userId)
        {
            if (_userPolicies.TryGetValue(userId, out UserPolicy policy))
            {
                return Task.FromResult(policy);
            }

            return Task.FromResult<UserPolicy>(null);
        }

        public Task SetUserPolicy(UserPolicy policy)
        {
            _userPolicies[policy.UserId] = policy;
            return Task.CompletedTask;
        }

        public Task<PathPolicy> GetPathPolicy(string path)
        {
            if (_pathPolicies.TryGetValue(path, out PathPolicy policy))
            {
                return Task.FromResult(policy);
            }

            return Task.FromResult<PathPolicy>(null);
        }

        public Task SetPathPolicy(PathPolicy policy)
        {
            _pathPolicies[policy.PathParts.ComposePath()] = policy;
            return Task.CompletedTask;
        }
    }
}
