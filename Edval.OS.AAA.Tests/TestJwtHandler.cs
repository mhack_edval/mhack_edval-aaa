﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Edval.OS.AAA.JWT;
using Edval.OS.AAA.JWT.PublicKeys;
using FluentAssertions;
using JsonWebToken;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestJwtHandler
    {
        [TestMethod]
        public async Task ValidJwt()
        {
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };
            DummyPublicKeySource publicKeys = await PublicKeyCache<DummyPublicKeySource>.GetKeys(JsonSerializer.Serialize(keys));
            string issuer = "tester";
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow + TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId,
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = issuer,
                SigningKey = signingKey
            };

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            JwtHandler handler = new JwtHandler(issuer, publicKeys);
            JwtHandler.Response result = handler.TryValidateToken(stringToken, SignatureAlgorithm.RsaSha256, out var token);
            result.Should().Be(JwtHandler.Response.Success);
            token.Should().NotBeNull();
            token.Issuer.Should().Be(issuer);
            token.IssuedAt.Should().Be(issuedAt);
            token.ExpirationTime.Should().Be(expirationTime);
        }

        [TestMethod]
        public async Task ExpiredJwt()
        {
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };
            DummyPublicKeySource publicKeys = await PublicKeyCache<DummyPublicKeySource>.GetKeys(JsonSerializer.Serialize(keys));
            string issuer = "tester";
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(2);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow - TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId,
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = issuer,
                SigningKey = signingKey
            };

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            JwtHandler handler = new JwtHandler(issuer, publicKeys);
            JwtHandler.Response result = handler.TryValidateToken(stringToken, SignatureAlgorithm.RsaSha256, out var token);
            result.Should().Be(JwtHandler.Response.InvalidToken);
            token.Should().BeNull();
        }

        [TestMethod]
        public async Task InvalidIssuer()
        {
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };
            DummyPublicKeySource publicKeys = await PublicKeyCache<DummyPublicKeySource>.GetKeys(JsonSerializer.Serialize(keys));
            string issuer = "tester";
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow + TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId,
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = issuer + "12",
                SigningKey = signingKey
            };

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            JwtHandler handler = new JwtHandler(issuer, publicKeys);
            JwtHandler.Response result = handler.TryValidateToken(stringToken, SignatureAlgorithm.RsaSha256, out var token);
            result.Should().Be(JwtHandler.Response.InvalidToken);
            token.Should().BeNull();
        }

        [TestMethod]
        public async Task InvalidSigningKey()
        {
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };
            DummyPublicKeySource publicKeys = await PublicKeyCache<DummyPublicKeySource>.GetKeys(JsonSerializer.Serialize(keys));
            string issuer = "tester";
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow + TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId,
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = issuer,
                SigningKey = signingKey
            };

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            JwtHandler handler = new JwtHandler(issuer, publicKeys);
            JwtHandler.Response result = handler.TryValidateToken(stringToken, SignatureAlgorithm.RsaSha256, out var token);
            result.Should().Be(JwtHandler.Response.InvalidToken);
            token.Should().BeNull();
        }

        [TestMethod]
        public async Task UnknownPublicKey()
        {
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };
            DummyPublicKeySource publicKeys = await PublicKeyCache<DummyPublicKeySource>.GetKeys(JsonSerializer.Serialize(keys));
            string issuer = "tester";
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow + TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId + "hey",
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = issuer,
                SigningKey = signingKey
            };

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            JwtHandler handler = new JwtHandler(issuer, publicKeys);
            JwtHandler.Response result = handler.TryValidateToken(stringToken, SignatureAlgorithm.RsaSha256, out var token);
            result.Should().Be(JwtHandler.Response.InvalidOrUnknownPublicKey);
            token.Should().BeNull();
        }

        [TestMethod]
        public async Task AlgorithmTypeNoneThrowsInvalidOperation()
        {
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };
            DummyPublicKeySource publicKeys = await PublicKeyCache<DummyPublicKeySource>.GetKeys(JsonSerializer.Serialize(keys));
            string issuer = "tester";
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow + TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId,
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = issuer,
                SigningKey = signingKey
            };

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            JwtHandler handler = new JwtHandler(issuer, publicKeys);

            Action tryValidate = () => handler.TryValidateToken(stringToken, SignatureAlgorithm.None, out var token);
            tryValidate.Should().Throw<ArgumentException>();
        }
    }
}
