﻿using System;
using Edval.OS.AAA.Database;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestVersionUtils
    {
        [TestMethod]
        public void CanConvertBackAndForth()
        {
            var version = new Version(100, 234, 1231, 12312);
            var versionLong = VersionUtils.VersionToULong(version);
            var versionConvertedBack = VersionUtils.ULongToVersion(versionLong);
            versionConvertedBack.Should().BeEquivalentTo(version);

            version = new Version(1, 1, 1, 1);
            versionLong = VersionUtils.VersionToULong(version);
            versionConvertedBack = VersionUtils.ULongToVersion(versionLong);
            versionConvertedBack.Should().BeEquivalentTo(version);

            version = new Version(9459, 5675, 234, 7890);
            versionLong = VersionUtils.VersionToULong(version);
            versionConvertedBack = VersionUtils.ULongToVersion(versionLong);
            versionConvertedBack.Should().BeEquivalentTo(version);

            version = new Version(0, 0, 0, 0);
            versionLong = VersionUtils.VersionToULong(version);
            versionConvertedBack = VersionUtils.ULongToVersion(versionLong);
            versionConvertedBack.Should().BeEquivalentTo(version);

            version = new Version(ushort.MaxValue, ushort.MaxValue, ushort.MaxValue, ushort.MaxValue);
            versionLong = VersionUtils.VersionToULong(version);
            versionConvertedBack = VersionUtils.ULongToVersion(versionLong);
            versionConvertedBack.Should().BeEquivalentTo(version);
        }
    }
}
