﻿using Edval.OS.AAA.Extensions;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestStringExtensions
    {
        [TestMethod]
        public void FromBase64Url()
        {
            const string NormalString = "I find your lack of faith disturbing.";
            const string Base64Url = "SSBmaW5kIHlvdXIgbGFjayBvZiBmYWl0aCBkaXN0dXJiaW5nLg";
            Base64Url.FromBase64UrlToString().Should().Be(NormalString);

            const string Base64Url2 =
                "pS9IqgTf4IF9OkzyyaCLdCdhbr0PnmmrbkiPTf9B2BzeAZWXxkVdVWUM1KfATZRSpLOvxdi2Pqk_2pVT-F6QJDb6CiGZ1RvL_WjK7chKHTTvQUvgqFNtDJy3fbpFyCqdlhg7pCuWZzEdNAI83boZuH8b8kKV58CRQc423VXmQKfdZVs_3DjhvI0p8QrXEhceEQ3f5tG6nj7m1iRDlKPDZ9mFqk1l0ANIisnFk9u5XdjVo6CT01yK7be-4sWzYPo3vK0SY1B45VkAOTZgLtRzdKb4LOWAKH-EWNjbl3RT3Gcqrc1SvGyGMVNhdfG1iBOdPtmOAqexZisB6UUA8OqIFw";
            Base64Url2.FromBase64UrlToString();
            Base64Url2.FromBase64UrlToByteArray();
        }
    }
}
