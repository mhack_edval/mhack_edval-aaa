﻿using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestPathPolicy
    {
        [TestMethod]
        public void Validation()
        {
            PolicyBuilder builder = new PolicyBuilder();
            PathPolicy policy = builder.BuildPathPolicyFromYaml(Policies.TimetableTeachersSubjectTeachersPathPolicy);
            (bool valid, string reason) = policy.Validate();
            valid.Should().BeTrue();
            reason.Should().BeNullOrEmpty();

            policy = new PathPolicy();
            (valid, reason) = policy.Validate();
            valid.Should().BeFalse();
            reason.Should().NotBeNullOrEmpty();
        }
    }
}
