﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Edval.OS.AAA.JWT.PublicKeys;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Services;
using FluentAssertions;
using JsonWebToken;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestRequestAuthoriser
    {
        private const string ValidIssuer = "TEST";

        [TestMethod]
        public async Task IsRequestAuthorised()
        {
            string keyId = "test";
            var signingKey = RsaJwk.GenerateKey(2048, true, SignatureAlgorithm.RsaSha256);
            PublicKey key = new PublicKey();
            key.EBytes = signingKey.E.ToArray();
            key.NBytes = signingKey.N.ToArray();
            key.Alg = "rs256";
            key.Use = "sig";
            key.KId = keyId;
            var keys = new Dictionary<string, PublicKey>()
            {
                { keyId, key}
            };

            var db = new DummyInMemoryAuthRepo();
            var policyBuilder = new PolicyBuilder();
            await db.SetPathPolicy(policyBuilder.BuildPathPolicyFromYaml(Policies.TimetableTeachersSubjectTeachersPathPolicy));
            var facultyHeadPolicy = policyBuilder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);
            await db.SetUserPolicy(facultyHeadPolicy);
            await PublicKeyCache<DummyPublicKeySource>.ClearCache();
            RequestAuthoriser<DummyPublicKeySource> authoriser = new RequestAuthoriser<DummyPublicKeySource>(ValidIssuer, JsonSerializer.Serialize(keys), db, new PolicyEnforcer());
            DateTime issuedAt = DateTime.UtcNow - TimeSpan.FromMinutes(1);
            issuedAt = new DateTime(issuedAt.Year, issuedAt.Month, issuedAt.Day, issuedAt.Hour, issuedAt.Minute, issuedAt.Second).ToUniversalTime();
            DateTime expirationTime = DateTime.UtcNow + TimeSpan.FromDays(1);
            expirationTime = new DateTime(expirationTime.Year, expirationTime.Month, expirationTime.Day, expirationTime.Hour, expirationTime.Minute, expirationTime.Second).ToUniversalTime();

            var descriptor = new JwsDescriptor()
            {
                Algorithm = SignatureAlgorithm.RsaSha256,
                KeyId = keyId,
                IssuedAt = issuedAt,
                ExpirationTime = expirationTime,
                Issuer = ValidIssuer,
                SigningKey = signingKey,
                Subject = facultyHeadPolicy.UserId,
            };

            descriptor.AddClaim("policy", Policies.FacultyHeadUserPolicy);

            var writer = new JwtWriter();
            string stringToken = writer.WriteTokenString(descriptor);

            Dictionary<string, string> headers = new Dictionary<string, string>()
            {
                { "TenantId", facultyHeadPolicy.TenantId },
                { "faculty", "b12d345c-3142-4254-a997-0156200db6a2" }
            };

            var authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeTrue();
            authoriseResult.jwt.Should().NotBeNull();

            // variable changes
            // slightly different
            headers["faculty"] = "b12d345c-3142-4254-a897-0156200db6a2"; 
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // empty
            headers["faculty"] = "";
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // null
            headers["faculty"] = null;
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // not there
            headers.Remove("faculty");
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // reset
            headers = new Dictionary<string, string>()
            {
                { "TenantId", facultyHeadPolicy.TenantId },
                { "faculty", "b12d345c-3142-4254-a997-0156200db6a2" }
            };

            // TenantId changes
            // slightly different
            headers["TenantId"] = facultyHeadPolicy.TenantId + "1"; 
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // empty
            headers["TenantId"] = "";
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // null
            headers["TenantId"] = null;
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // reset
            headers = new Dictionary<string, string>()
            {
                { "TenantId", facultyHeadPolicy.TenantId },
                { "faculty", "b12d345c-3142-4254-a997-0156200db6a2" }
            };

            // test reset
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeTrue();
            authoriseResult.jwt.Should().NotBeNull();

            // slightly different path
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teacher", stringToken, HttpMethod.GET, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();

            // different http method
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.PUT, headers));
            authoriseResult.success.Should().BeTrue();
            authoriseResult.jwt.Should().NotBeNull();

            // different http method
            authoriseResult = await authoriser.IsRequestAuthorised(
                new Request("/timetable/teachers/subject-teachers", stringToken, HttpMethod.TRACE, headers));
            authoriseResult.success.Should().BeFalse();
            authoriseResult.jwt.Should().BeNull();
        }
    }
}
