﻿using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Edval.OS.AAA.JWT.PublicKeys;

namespace Edval.OS.AAA.Tests
{
    public class DummyPublicKeySource : IPublicKeys
    {
        public IReadOnlyDictionary<string, PublicKey> Keys { get; set; }

        // url is actually the keys here...
        public Task InitAsync(string url)
        {
            Keys = JsonSerializer.Deserialize<Dictionary<string, PublicKey>>(url);
            return Task.CompletedTask;
        }
    }
}
