using System.Collections.Generic;
using Edval.OS.AAA.Models;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestPathPart
    {
        [TestMethod]
        public void DecomposePath()
        {
            IReadOnlyList<PathPart> parts = PathPart.DecomposePath("/hello/there/hmm4/astasd@");
            Assert.AreEqual(4, parts.Count);
            Assert.AreEqual("hello", parts[0].Value);
            Assert.AreEqual("there", parts[1].Value);
            Assert.AreEqual("hmm4", parts[2].Value);
            Assert.AreEqual("astasd@", parts[3].Value);

            parts = PathPart.DecomposePath("hello/there/hmm4/astasd@");
            parts.Count.Should().Be(4);
            parts[0].Value.Should().Be("hello");
            parts[1].Value.Should().Be("there");
            parts[2].Value.Should().Be("hmm4");
            parts[3].Value.Should().Be("astasd@");

            parts = PathPart.DecomposePath("/hello/there?general/grevious");
            parts.Count.Should().Be(2);
            parts[0].Value.Should().Be("hello");
            parts[1].Value.Should().Be("there");

            parts = PathPart.DecomposePath("/hello/there#general/grevious");
            parts.Count.Should().Be(2);
            parts[0].Value.Should().Be("hello");
            parts[1].Value.Should().Be("there");
        }

        [TestMethod]
        public void Validation()
        {
            PathPart part = new PathPart();
            (bool valid, string reason) = part.Validate();
            valid.Should().BeFalse();
            reason.Should().NotBeNullOrEmpty();

            IReadOnlyList<PathPart> parts = PathPart.DecomposePath("/hello/there/hmm4m/astasd@");
            foreach (PathPart pathPart in parts)
            {
                (valid, reason) = pathPart.Validate();
                valid.Should().BeTrue();
                reason.Should().BeNullOrEmpty();
            }
        }
    }
}
