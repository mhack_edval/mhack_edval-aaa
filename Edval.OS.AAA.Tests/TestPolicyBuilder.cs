﻿using System.Collections.Generic;
using System.Linq;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Models.User;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestPolicyBuilder
    {
        [TestMethod]
        public void BuildUserPolicyFromYaml()
        {
            PolicyBuilder builder = new PolicyBuilder();
            UserPolicy policy = builder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);

            policy.Version.Should().Be("v1");
            policy.Name.Should().Be("faculty-head");
            policy.UserId.Should().Be("50f14c48-b7ef-4b1c-97fa-afaaa0dc0f43");
            policy.TenantId.Should().Be("ebb19c7e-7f36-4475-9fd5-80ec0b334fbb");

            policy.GetUserPolicyCorrespondingToPath(
                    PathPart.DecomposePath("/timetable/teachers/subject-teachers/{faculty}/b12d345c-3142-4254-a997-0156200db6a2"))
                .GrantedPermission.Should().Be(Permission.Write);

            policy.AllowedPaths.GetChild("timetable").Children.GetChild("teachers").Children
                .GetChild("timetable").Children.GetChild("{faculty}").Children
                .GetChild("b12d345c-3142-4254-a997-0156200db6a2").GrantedPermission.Should().Be(Permission.Write);

            policy.AllowedPaths.GetChild("timetable").Children.GetChild("teachers").Children
                .GetChild("timetable").Children.GetChild("{faculty}").Children
                .GetChild("4f0b4b36-ada6-4c87-9f01-96df12724399").GrantedPermission.Should().Be(Permission.Read);

            policy.AllowedPaths.GetChild("timetable").Children.GetChild("rooms").Children
                .GetChild("subject-rooms").Children.GetChild("{faculty}").Children
                .GetChild("b12d345c-3142-4254-a997-0156200db6a2").GrantedPermission.Should().Be(Permission.Write);
        }

        [TestMethod]
        public void BuildPathPolicyFromYaml()
        {
            PolicyBuilder builder = new PolicyBuilder();
            PathPolicy policy = builder.BuildPathPolicyFromYaml(Policies.TimetableTeachersSubjectTeachersPathPolicy);
            List<PathPart> path = PathPart.DecomposePath("/timetable/teachers/subject-teachers");

            policy.Version.Should().Be("v1");
            policy.Path.Count.Should().Be(path.Count);

            for (int i = 0; i < policy.Path.Count; i++)
            {
                policy.Path[i].Should().Be(path[i]);
            }

            policy.PathPolicyVariables.Single(x => x.PolicyKey == "TenantId").RequestKey.Should().BeEquivalentTo("TenantId");
            policy.PathPolicyVariables.Single(x => x.PolicyKey == "faculty").Location.Should().Be(PathPolicyVariableLocation.Header);
            policy.PathPolicyMethods.Single(x => x.HttpMethod == HttpMethod.GET).Requirements[0].RequiredPermission
                .Should().Be(Permission.Read);
            policy.PathPolicyMethods.Single(x => x.HttpMethod == HttpMethod.PUT).Requirements[0].RequiredPermission
                .Should().Be(Permission.Write);

            path = PathPart.DecomposePath("timetable.teachers.subject-teachers.$faculty$", '.');
            IReadOnlyList<PathPart> requirementPath = policy.PathPolicyMethods.Single(x => x.HttpMethod == HttpMethod.PUT)
                .Requirements[0].PathParts;
            requirementPath.Count.Should().Be(path.Count);
            for (int i = 0; i < policy.Path.Count; i++)
            {
                requirementPath[i].Should().Be(path[i]);
            }
        }
    }
}
