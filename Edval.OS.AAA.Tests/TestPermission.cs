﻿using Edval.OS.AAA.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestPermission
    {
        /// <summary>
        /// If this test fails then permissions are completely broken.
        /// </summary>
        [TestMethod]
        public void TestPermissionLevelsAreCorrect()
        {
            Assert.IsTrue(Permission.None < Permission.Read);
            Assert.IsTrue(Permission.Read < Permission.Write);
            Assert.IsTrue(Permission.Write < Permission.All);
        }
    }
}
