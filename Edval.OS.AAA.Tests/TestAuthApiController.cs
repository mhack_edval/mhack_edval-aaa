using System.Collections.Generic;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.User;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AuthApiController = Edval.OS.AAA.Controllers.AuthApiController;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestAuthApiController
    {
        [TestMethod]
        public void AppendUserPolicyPaths()
        {
            var list = new List<UserPolicyPath>();
            AuthApiController.AppendUserPolicyPaths(list, new List<string>()
            {
                "wizard/user",
                "wizard/dummy",
                "internal"
            }, Permission.Read);
            
            AuthApiController.AppendUserPolicyPaths(list, new List<string>()
            {
                "wizard/user/demo",
                "wizard/dummy/long/path",
            }, Permission.Write);

            list.Count.Should().Be(2);
            
            list.TryGetChild("wizard", out UserPolicyPath wizardPolicyPath).Should().BeTrue();
            wizardPolicyPath.Children.Count.Should().Be(2);
            wizardPolicyPath.Children[0].Path.Should().Be("user");
            wizardPolicyPath.Children[1].Path.Should().Be("dummy");
            wizardPolicyPath.Children[0].GrantedPermission.Should().Be(Permission.Read);
            wizardPolicyPath.Children[1].GrantedPermission.Should().Be(Permission.Read);

            wizardPolicyPath.Children.TryGetChild("user", out UserPolicyPath userPath).Should().BeTrue();
            userPath.Children.TryGetChild("demo", out userPath).Should().BeTrue();
            userPath.GrantedPermission.Should().Be(Permission.Write);
            
            wizardPolicyPath.Children.TryGetChild("dummy", out UserPolicyPath dummyPath).Should().BeTrue();
            dummyPath.Children.TryGetChild("long", out dummyPath).Should().BeTrue();
            dummyPath.Children.TryGetChild("path", out dummyPath).Should().BeTrue();
            dummyPath.GrantedPermission.Should().Be(Permission.Write);
            
            list.TryGetChild("internal", out UserPolicyPath internalPolicy).Should().BeTrue();
            internalPolicy.GrantedPermission.Should().Be(Permission.Read);
        }
    }
}
