﻿using System.Diagnostics.CodeAnalysis;
using Edval.OS.AAA.Extensions;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.User;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestUserPolicy
    {
        [TestMethod]
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        public void ResolvePath()
        {
            PolicyBuilder builder = new PolicyBuilder();
            UserPolicy policy = builder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);

            policy.AllowedPaths.GetChild("timetable").Children.GetChild("teachers").Children
                .GetChild("subject-teachers").Children.GetChild("{faculty}").Children
                .GetChild("b12d345c-3142-4254-a997-0156200db6a2")
                .Should()
                .Be(policy.GetUserPolicyCorrespondingToPath(PathPart.DecomposePath(
                    "/timetable/teachers/subject-teachers/{faculty}/b12d345c-3142-4254-a997-0156200db6a2")));

            policy.AllowedPaths.GetChild("timetable").Children.GetChild("rooms").Children
                .GetChild("subject-rooms").Children.GetChild("{faculty}").Children
                .GetChild("b12d345c-3142-4254-a997-0156200db6a2")
                .Should()
                .Be(policy.GetUserPolicyCorrespondingToPath(PathPart.DecomposePath(
                        "/timetable/rooms/subject-rooms/{faculty}/b12d345c-3142-4254-a997-0156200db6a2")));
        }

        [TestMethod]
        public void Validation()
        {
            PolicyBuilder builder = new PolicyBuilder();
            UserPolicy policy = builder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);
            (bool valid, string reason) = policy.Validate();
            valid.Should().BeTrue();
            reason.Should().BeNullOrEmpty();

            policy = new UserPolicy();
            (valid, reason) = policy.Validate();
            valid.Should().BeFalse();
            reason.Should().NotBeNullOrEmpty();
        }
    }
}
