﻿namespace Edval.OS.AAA.Tests
{
    public static class Policies
    {
        public const string TimetablerUserPolicy = @"
policy:
  version: v1
  name: faculty-head
  user-id: 50f14c48-b7ef-4b1c-97fa-afaaa0dc0f43
  tenant-id: ebb19c7e-7f36-4475-9fd5-80ec0b334fbb
  allow:
    timetable: write
";

        public const string FacultyHeadUserPolicy = @"
policy:
  version: v1
  name: faculty-head
  user-id: 50f14c48-b7ef-4b1c-97fa-afaaa0dc0f43
  tenant-id: ebb19c7e-7f36-4475-9fd5-80ec0b334fbb
  allow:
    timetable:
      teachers:
        subject-teachers:
          '{faculty}':
            - b12d345c-3142-4254-a997-0156200db6a2: write
        timetable:
          '{faculty}':
            - b12d345c-3142-4254-a997-0156200db6a2: write
            - 4f0b4b36-ada6-4c87-9f01-96df12724399: read
      rooms:
        subject-rooms:
          '{faculty}':
            - b12d345c-3142-4254-a997-0156200db6a2: write";

        public const string TimetableTeachersSubjectTeachersPathPolicy = @"
policy:
  version: v1
  path: /timetable/teachers/subject-teachers
  variables:
    - key: TenantId
      location: header
    - key: faculty
      location: header
  allow:
    - method: GET
      requires:
        - path: timetable.teachers.subject-teachers.{faculty}
          level: read
    - method: PUT
      requires:
        - path: timetable.teachers.subject-teachers.{faculty}
          level: write
    - method: DELETE
      requires:
        - path: timetable.teachers.subject-teachers.{faculty}
          level: write";
    }
}
