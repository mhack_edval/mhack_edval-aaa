﻿using System.Collections.Generic;
using Edval.OS.AAA.Models;
using Edval.OS.AAA.Models.Path;
using Edval.OS.AAA.Services;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Edval.OS.AAA.Tests
{
    [TestClass]
    public class TestPolicyResolver
    {
        [TestMethod]
        public void IsAccessAllowedPolicyPermissions()
        {
            PolicyBuilder builder = new PolicyBuilder();
            var timetablerUserPolicy = builder.BuildUserPolicyFromYaml(Policies.TimetablerUserPolicy);
            var facultyHeadUserPolicy = builder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);
            var pathPolicy = builder.BuildPathPolicyFromYaml(Policies.TimetableTeachersSubjectTeachersPathPolicy);
            PolicyEnforcer policyEnforcer = new PolicyEnforcer();

            bool accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeTrue();

            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeFalse();

            accessAllowed = policyEnforcer.IsAccessAllowed(timetablerUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeTrue();
        }

        [TestMethod]
        public void IsAccessAllowedTenantId()
        {
            PolicyBuilder builder = new PolicyBuilder();
            var facultyHeadUserPolicy = builder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);
            var pathPolicy = builder.BuildPathPolicyFromYaml(Policies.TimetableTeachersSubjectTeachersPathPolicy);
            PolicyEnforcer policyEnforcer = new PolicyEnforcer();

            // tenantId invalid
            bool accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "afaaa0dc0f43" }
                });
            accessAllowed.Should().BeFalse();

            // tenantId one letter difference
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fba" }
                });
            accessAllowed.Should().BeFalse();

            // tenantId valid
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2" },
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeTrue();

            // tenantId empty
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2" },
                    {"TenantId", "" }
                });
            accessAllowed.Should().BeFalse();

            // tenantId null
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2" },
                    {"TenantId", null }
                });
            accessAllowed.Should().BeFalse();
        }

        [TestMethod]
        public void IsAccessAllowedHttpMethod()
        {
            PolicyBuilder builder = new PolicyBuilder();
            var facultyHeadUserPolicy = builder.BuildUserPolicyFromYaml(Policies.FacultyHeadUserPolicy);
            var pathPolicy = builder.BuildPathPolicyFromYaml(Policies.TimetableTeachersSubjectTeachersPathPolicy);
            PolicyEnforcer policyEnforcer = new PolicyEnforcer();

            // correct 
            bool accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.GET), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeTrue();

            // none
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.None), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeFalse();

            // (HttpMethod) (-1) - invalid value
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod((HttpMethod) (-1)), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeFalse();

            // HttpMethod.Patch - not on policy
            accessAllowed = policyEnforcer.IsAccessAllowed(facultyHeadUserPolicy,
                pathPolicy.FindPathPolicyMethod(HttpMethod.PATCH), new Dictionary<string, string>()
                {
                    {"faculty", "b12d345c-3142-4254-a997-0156200db6a2"},
                    {"TenantId", "ebb19c7e-7f36-4475-9fd5-80ec0b334fbb" }
                });
            accessAllowed.Should().BeFalse();
        }
    }
}
